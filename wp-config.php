<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cycling');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'password');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '{aZJ2PRuV~?vGRwy+g_tdKXw}.X4_6Jdw0>iC/>!w,]705}57I({f=aQ9jpUwWm/');
define('SECURE_AUTH_KEY',  'qSH}DKlWHpvuIv:Dd1R10hS|%PC,]:{(&mJo:a]*&VlB5QZQPC}C+<p[5y)ch1:x');
define('LOGGED_IN_KEY',    'NAJ2db|4x#%YMdOUJQ@@zSRNGbIF rWodh%lLZ~>#XDgRPPyx[^Kzg}m<jZtxbW5');
define('NONCE_KEY',        '0Nepd:LH8ioPo[-PNXn3Za;6MM)pM-Aplq*e.^EylO-jv^#|ruOP(i/?tc.`/Na0');
define('AUTH_SALT',        'L`N$&AD0yReRU~HLP354n)hF;RLbUN-g4],vupm8=,eP.`eXw=QmibwI<Blj%e}|');
define('SECURE_AUTH_SALT', 'oV_]b;<+C>]fKNcTOY`I3?urKk=_R:6YRXb@9F7M(uB >]Khbec(SxyB,CM8A0M)');
define('LOGGED_IN_SALT',   'Vr_AZzx8P;nq`g?35g#Q<`CvI_dy=5cNtn~%4c^xj~S.-m%^%oeP}f|E{|^rP-j3');
define('NONCE_SALT',       'AUTe6LJk_?p5J`a]Z_^eK %BsmIm_L0.hQFJupjz_!1:2t:^<T.tmCR>7rf[1go+');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'web_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
