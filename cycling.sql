-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 02, 2018 at 03:57 PM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 7.0.28-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cycling`
--

-- --------------------------------------------------------

--
-- Table structure for table `web_commentmeta`
--

CREATE TABLE `web_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `web_comments`
--

CREATE TABLE `web_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `web_comments`
--

INSERT INTO `web_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2018-03-07 08:33:38', '2018-03-07 08:33:38', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href="https://gravatar.com">Gravatar</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `web_links`
--

CREATE TABLE `web_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `web_options`
--

CREATE TABLE `web_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `web_options`
--

INSERT INTO `web_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/office_projects/cycling', 'yes'),
(2, 'home', 'http://localhost/office_projects/cycling', 'yes'),
(3, 'blogname', 'Himalaya Biking', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'pracriti11neupane@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:112:{s:11:"^wp-json/?$";s:22:"index.php?rest_route=/";s:14:"^wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:21:"^index.php/wp-json/?$";s:22:"index.php?rest_route=/";s:24:"^index.php/wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:9:"events/?$";s:26:"index.php?post_type=events";s:39:"events/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?post_type=events&feed=$matches[1]";s:34:"events/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?post_type=events&feed=$matches[1]";s:26:"events/page/([0-9]{1,})/?$";s:44:"index.php?post_type=events&paged=$matches[1]";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:23:"category/(.+?)/embed/?$";s:46:"index.php?category_name=$matches[1]&embed=true";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:20:"tag/([^/]+)/embed/?$";s:36:"index.php?tag=$matches[1]&embed=true";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:21:"type/([^/]+)/embed/?$";s:44:"index.php?post_format=$matches[1]&embed=true";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:34:"events/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:44:"events/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:64:"events/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"events/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"events/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:40:"events/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:23:"events/([^/]+)/embed/?$";s:39:"index.php?events=$matches[1]&embed=true";s:27:"events/([^/]+)/trackback/?$";s:33:"index.php?events=$matches[1]&tb=1";s:47:"events/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:45:"index.php?events=$matches[1]&feed=$matches[2]";s:42:"events/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:45:"index.php?events=$matches[1]&feed=$matches[2]";s:35:"events/([^/]+)/page/?([0-9]{1,})/?$";s:46:"index.php?events=$matches[1]&paged=$matches[2]";s:31:"events/([^/]+)(?:/([0-9]+))?/?$";s:45:"index.php?events=$matches[1]&page=$matches[2]";s:23:"events/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:33:"events/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:53:"events/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:48:"events/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:48:"events/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:29:"events/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:8:"embed/?$";s:21:"index.php?&embed=true";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:27:"comment-page-([0-9]{1,})/?$";s:38:"index.php?&page_id=9&cpage=$matches[1]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:17:"comments/embed/?$";s:21:"index.php?&embed=true";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:20:"search/(.+)/embed/?$";s:34:"index.php?s=$matches[1]&embed=true";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:23:"author/([^/]+)/embed/?$";s:44:"index.php?author_name=$matches[1]&embed=true";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:45:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$";s:74:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:32:"([0-9]{4})/([0-9]{1,2})/embed/?$";s:58:"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:19:"([0-9]{4})/embed/?$";s:37:"index.php?year=$matches[1]&embed=true";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:58:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:68:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:88:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:83:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:83:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:64:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:53:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$";s:91:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$";s:85:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1";s:77:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]";s:72:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]";s:65:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$";s:98:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]";s:72:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$";s:98:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]";s:61:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]";s:47:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:57:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:77:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:72:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:72:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:53:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]";s:51:"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]";s:38:"([0-9]{4})/comment-page-([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&cpage=$matches[2]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:".?.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"(.?.+?)/embed/?$";s:41:"index.php?pagename=$matches[1]&embed=true";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:24:"(.?.+?)(?:/([0-9]+))?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:2:{i:0;s:34:"advanced-custom-fields-pro/acf.php";i:1;s:36:"contact-form-7/wp-contact-form-7.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'himalaya-biking', 'yes'),
(41, 'stylesheet', 'himalaya-biking', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '9', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '38590', 'yes'),
(92, 'web_user_roles', 'a:5:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:61:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}}', 'yes'),
(93, 'fresh_site', '0', 'yes'),
(94, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(95, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(96, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(97, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(98, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(99, 'sidebars_widgets', 'a:2:{s:19:"wp_inactive_widgets";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:13:"array_version";i:3;}', 'yes'),
(100, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(101, 'widget_calendar', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(102, 'widget_media_audio', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(103, 'widget_media_image', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(104, 'widget_media_gallery', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(105, 'widget_media_video', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(106, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(107, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(108, 'widget_custom_html', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(109, 'cron', 'a:4:{i:1521966819;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1521970483;a:2:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}s:25:"delete_expired_transients";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1521971232;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}s:7:"version";i:2;}', 'yes'),
(110, 'theme_mods_twentyseventeen', 'a:2:{s:18:"custom_css_post_id";i:-1;s:16:"sidebars_widgets";a:2:{s:4:"time";i:1520415540;s:4:"data";a:4:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:9:"sidebar-2";a:0:{}s:9:"sidebar-3";a:0:{}}}}', 'yes'),
(119, '_site_transient_update_themes', 'O:8:"stdClass":4:{s:12:"last_checked";i:1521960121;s:7:"checked";a:4:{s:15:"himalaya-biking";s:0:"";s:13:"twentyfifteen";s:3:"1.9";s:15:"twentyseventeen";s:3:"1.4";s:13:"twentysixteen";s:3:"1.4";}s:8:"response";a:0:{}s:12:"translations";a:0:{}}', 'no'),
(124, '_site_transient_update_core', 'O:8:"stdClass":4:{s:7:"updates";a:1:{i:0;O:8:"stdClass":10:{s:8:"response";s:6:"latest";s:8:"download";s:59:"https://downloads.wordpress.org/release/wordpress-4.9.4.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:59:"https://downloads.wordpress.org/release/wordpress-4.9.4.zip";s:10:"no_content";s:70:"https://downloads.wordpress.org/release/wordpress-4.9.4-no-content.zip";s:11:"new_bundled";s:71:"https://downloads.wordpress.org/release/wordpress-4.9.4-new-bundled.zip";s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.9.4";s:7:"version";s:5:"4.9.4";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.7";s:15:"partial_version";s:0:"";}}s:12:"last_checked";i:1521960114;s:15:"version_checked";s:5:"4.9.4";s:12:"translations";a:0:{}}', 'no'),
(125, 'can_compress_scripts', '0', 'no'),
(128, 'auto_core_update_notified', 'a:4:{s:4:"type";s:7:"success";s:5:"email";s:27:"pracriti11neupane@gmail.com";s:7:"version";s:5:"4.9.4";s:9:"timestamp";i:1520415294;}', 'no'),
(140, 'current_theme', 'Himalaya Biking', 'yes'),
(141, 'theme_mods_himalaya-biking', 'a:2:{i:0;b:0;s:18:"nav_menu_locations";a:1:{s:7:"primary";i:2;}}', 'yes'),
(142, 'theme_switched', '', 'yes'),
(146, 'recently_activated', 'a:0:{}', 'yes'),
(147, 'acf_version', '5.4.6', 'yes'),
(156, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:"auto_add";a:0:{}}', 'yes'),
(157, 'options_header_logo', '33', 'no'),
(158, '_options_header_logo', 'field_5a9fd01c0e411', 'no'),
(159, 'options_footer_logo', '34', 'no'),
(160, '_options_footer_logo', 'field_5a9fd037ed62a', 'no'),
(162, 'options_address', 'Demo Address, Demo Street 123, State', 'no'),
(163, '_options_address', 'field_5a9fd2c3d0d43', 'no'),
(164, 'options_email', 'outdoornagarkot@gmail.com', 'no'),
(165, '_options_email', 'field_5a9fd2e70c00a', 'no'),
(166, 'options_phone_number', ' +977-9851192617, 9851150976', 'no'),
(167, '_options_phone_number', 'field_5a9fd2f6d9fd0', 'no'),
(168, 'options_facebook_link', 'https://www.facebook.com/', 'no'),
(169, '_options_facebook_link', 'field_5a9fd3169d613', 'no'),
(170, 'options_twitter_link', 'https://www.twitter.com/', 'no'),
(171, '_options_twitter_link', 'field_5a9fd32d6577a', 'no'),
(172, 'options_instagram_link', 'https://www.instagram.com/', 'no'),
(173, '_options_instagram_link', 'field_5a9fd346c535a', 'no'),
(174, 'options_google_plus_link', 'https://plus.google.com', 'no'),
(175, '_options_google_plus_link', 'field_5a9fd35c80e04', 'no'),
(181, 'category_children', 'a:0:{}', 'yes'),
(182, 'options_sidebar_image', '75', 'no'),
(183, '_options_sidebar_image', 'field_5aa0d5ab7acfd', 'no'),
(190, 'wpcf7', 'a:2:{s:7:"version";s:5:"5.0.1";s:13:"bulk_validate";a:4:{s:9:"timestamp";i:1520494129;s:7:"version";s:5:"5.0.1";s:11:"count_valid";i:1;s:13:"count_invalid";i:0;}}', 'yes'),
(220, '_transient_timeout_acf_get_remote_plugin_info', '1522046517', 'no'),
(221, '_transient_acf_get_remote_plugin_info', 'a:13:{s:4:"name";s:26:"Advanced Custom Fields PRO";s:4:"slug";s:26:"advanced-custom-fields-pro";s:8:"homepage";s:37:"https://www.advancedcustomfields.com/";s:7:"version";s:5:"5.6.9";s:6:"author";s:13:"Elliot Condon";s:10:"author_url";s:28:"http://www.elliotcondon.com/";s:12:"contributors";s:12:"elliotcondon";s:8:"requires";s:5:"3.6.0";s:6:"tested";s:5:"4.9.9";s:6:"tagged";s:61:"acf, advanced, custom, field, fields, form, repeater, content";s:9:"changelog";s:505:"<h4>5.6.9</h4><ul><li>User field: Added new <code>Return Format</code> setting (Array, Object, ID)</li><li>Core: Added basic compatibility with Gutenberg - values now save</li><li>Core: Fixed bug affecting the loading of fields on new Menu Items</li><li>Core: Removed private (<code>show_ui</code> => false) post types from the <code>Post Type</code> location rule choices</li><li>Core: Minor fixes and improvements</li><li>Language: Updated French translation - thanks to Maxime Bernard-Jacquet</li></ul>";s:14:"upgrade_notice";s:0:"";s:5:"icons";a:1:{s:7:"default";s:63:"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png";}}', 'no'),
(222, '_site_transient_timeout_theme_roots', '1521961919', 'no'),
(223, '_site_transient_theme_roots', 'a:4:{s:15:"himalaya-biking";s:7:"/themes";s:13:"twentyfifteen";s:7:"/themes";s:15:"twentyseventeen";s:7:"/themes";s:13:"twentysixteen";s:7:"/themes";}', 'no'),
(224, '_site_transient_update_plugins', 'O:8:"stdClass":5:{s:12:"last_checked";i:1521960123;s:7:"checked";a:4:{s:34:"advanced-custom-fields-pro/acf.php";s:5:"5.4.6";s:19:"akismet/akismet.php";s:5:"4.0.1";s:36:"contact-form-7/wp-contact-form-7.php";s:5:"5.0.1";s:9:"hello.php";s:3:"1.6";}s:8:"response";a:2:{s:19:"akismet/akismet.php";O:8:"stdClass":11:{s:2:"id";s:21:"w.org/plugins/akismet";s:4:"slug";s:7:"akismet";s:6:"plugin";s:19:"akismet/akismet.php";s:11:"new_version";s:5:"4.0.3";s:3:"url";s:38:"https://wordpress.org/plugins/akismet/";s:7:"package";s:56:"https://downloads.wordpress.org/plugin/akismet.4.0.3.zip";s:5:"icons";a:3:{s:2:"1x";s:59:"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272";s:2:"2x";s:59:"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272";s:7:"default";s:59:"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272";}s:7:"banners";a:2:{s:2:"1x";s:61:"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904";s:7:"default";s:61:"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904";}s:11:"banners_rtl";a:0:{}s:6:"tested";s:5:"4.9.4";s:13:"compatibility";O:8:"stdClass":0:{}}s:34:"advanced-custom-fields-pro/acf.php";O:8:"stdClass":5:{s:4:"slug";s:26:"advanced-custom-fields-pro";s:6:"plugin";s:34:"advanced-custom-fields-pro/acf.php";s:11:"new_version";s:5:"5.6.9";s:3:"url";s:37:"https://www.advancedcustomfields.com/";s:7:"package";s:0:"";}}s:12:"translations";a:0:{}s:9:"no_update";a:2:{s:36:"contact-form-7/wp-contact-form-7.php";O:8:"stdClass":9:{s:2:"id";s:28:"w.org/plugins/contact-form-7";s:4:"slug";s:14:"contact-form-7";s:6:"plugin";s:36:"contact-form-7/wp-contact-form-7.php";s:11:"new_version";s:5:"5.0.1";s:3:"url";s:45:"https://wordpress.org/plugins/contact-form-7/";s:7:"package";s:63:"https://downloads.wordpress.org/plugin/contact-form-7.5.0.1.zip";s:5:"icons";a:3:{s:2:"1x";s:66:"https://ps.w.org/contact-form-7/assets/icon-128x128.png?rev=984007";s:2:"2x";s:66:"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007";s:7:"default";s:66:"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007";}s:7:"banners";a:3:{s:2:"2x";s:69:"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901";s:2:"1x";s:68:"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427";s:7:"default";s:69:"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901";}s:11:"banners_rtl";a:0:{}}s:9:"hello.php";O:8:"stdClass":9:{s:2:"id";s:25:"w.org/plugins/hello-dolly";s:4:"slug";s:11:"hello-dolly";s:6:"plugin";s:9:"hello.php";s:11:"new_version";s:3:"1.6";s:3:"url";s:42:"https://wordpress.org/plugins/hello-dolly/";s:7:"package";s:58:"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip";s:5:"icons";a:3:{s:2:"1x";s:63:"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=969907";s:2:"2x";s:63:"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=969907";s:7:"default";s:63:"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=969907";}s:7:"banners";a:2:{s:2:"1x";s:65:"https://ps.w.org/hello-dolly/assets/banner-772x250.png?rev=478342";s:7:"default";s:65:"https://ps.w.org/hello-dolly/assets/banner-772x250.png?rev=478342";}s:11:"banners_rtl";a:0:{}}}}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `web_postmeta`
--

CREATE TABLE `web_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `web_postmeta`
--

INSERT INTO `web_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(4, 5, '_edit_last', '1'),
(5, 5, '_edit_lock', '1520425516:1'),
(6, 9, '_edit_last', '1'),
(7, 9, '_edit_lock', '1520488290:1'),
(8, 11, '_menu_item_type', 'post_type'),
(9, 11, '_menu_item_menu_item_parent', '0'),
(10, 11, '_menu_item_object_id', '9'),
(11, 11, '_menu_item_object', 'page'),
(12, 11, '_menu_item_target', ''),
(13, 11, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(14, 11, '_menu_item_xfn', ''),
(15, 11, '_menu_item_url', ''),
(17, 2, '_wp_trash_meta_status', 'publish'),
(18, 2, '_wp_trash_meta_time', '1520416915'),
(19, 2, '_wp_desired_post_slug', 'sample-page'),
(20, 13, '_wp_attached_file', '2018/03/banner5.jpg'),
(21, 13, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1600;s:6:"height";i:750;s:4:"file";s:19:"2018/03/banner5.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"banner5-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"banner5-300x141.jpg";s:5:"width";i:300;s:6:"height";i:141;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:19:"banner5-768x360.jpg";s:5:"width";i:768;s:6:"height";i:360;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"banner5-1024x480.jpg";s:5:"width";i:1024;s:6:"height";i:480;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(22, 14, '_wp_attached_file', '2018/03/banner4.jpg'),
(23, 14, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1600;s:6:"height";i:750;s:4:"file";s:19:"2018/03/banner4.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"banner4-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"banner4-300x141.jpg";s:5:"width";i:300;s:6:"height";i:141;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:19:"banner4-768x360.jpg";s:5:"width";i:768;s:6:"height";i:360;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"banner4-1024x480.jpg";s:5:"width";i:1024;s:6:"height";i:480;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(24, 15, '_wp_attached_file', '2018/03/banner3.jpg'),
(25, 15, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1600;s:6:"height";i:750;s:4:"file";s:19:"2018/03/banner3.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"banner3-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"banner3-300x141.jpg";s:5:"width";i:300;s:6:"height";i:141;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:19:"banner3-768x360.jpg";s:5:"width";i:768;s:6:"height";i:360;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"banner3-1024x480.jpg";s:5:"width";i:1024;s:6:"height";i:480;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(26, 16, '_wp_attached_file', '2018/03/banner2.jpg'),
(27, 16, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1600;s:6:"height";i:750;s:4:"file";s:19:"2018/03/banner2.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"banner2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"banner2-300x141.jpg";s:5:"width";i:300;s:6:"height";i:141;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:19:"banner2-768x360.jpg";s:5:"width";i:768;s:6:"height";i:360;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"banner2-1024x480.jpg";s:5:"width";i:1024;s:6:"height";i:480;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(28, 17, '_wp_attached_file', '2018/03/banner1.jpg'),
(29, 17, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1600;s:6:"height";i:750;s:4:"file";s:19:"2018/03/banner1.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"banner1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"banner1-300x141.jpg";s:5:"width";i:300;s:6:"height";i:141;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:19:"banner1-768x360.jpg";s:5:"width";i:768;s:6:"height";i:360;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"banner1-1024x480.jpg";s:5:"width";i:1024;s:6:"height";i:480;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(30, 9, 'slider_0_slider_image', '13'),
(31, 9, '_slider_0_slider_image', 'field_5a9fb5845d1f0'),
(32, 9, 'slider_0_slider_content', 'Never Stop Exploring'),
(33, 9, '_slider_0_slider_content', 'field_5a9fb5ab5d1f1'),
(34, 9, 'slider_1_slider_image', '14'),
(35, 9, '_slider_1_slider_image', 'field_5a9fb5845d1f0'),
(36, 9, 'slider_1_slider_content', 'Never Stop Exploring'),
(37, 9, '_slider_1_slider_content', 'field_5a9fb5ab5d1f1'),
(38, 9, 'slider_2_slider_image', '15'),
(39, 9, '_slider_2_slider_image', 'field_5a9fb5845d1f0'),
(40, 9, 'slider_2_slider_content', 'Never Stop Exploring'),
(41, 9, '_slider_2_slider_content', 'field_5a9fb5ab5d1f1'),
(42, 9, 'slider_3_slider_image', '16'),
(43, 9, '_slider_3_slider_image', 'field_5a9fb5845d1f0'),
(44, 9, 'slider_3_slider_content', 'Never Stop Exploring'),
(45, 9, '_slider_3_slider_content', 'field_5a9fb5ab5d1f1'),
(46, 9, 'slider_4_slider_image', '17'),
(47, 9, '_slider_4_slider_image', 'field_5a9fb5845d1f0'),
(48, 9, 'slider_4_slider_content', 'Never Stop Exploring'),
(49, 9, '_slider_4_slider_content', 'field_5a9fb5ab5d1f1'),
(50, 9, 'slider', '5'),
(51, 9, '_slider', 'field_5a9fb5405d1ef'),
(52, 18, 'slider_0_slider_image', '13'),
(53, 18, '_slider_0_slider_image', 'field_5a9fb5845d1f0'),
(54, 18, 'slider_0_slider_content', 'Never Stop Exploring'),
(55, 18, '_slider_0_slider_content', 'field_5a9fb5ab5d1f1'),
(56, 18, 'slider_1_slider_image', '14'),
(57, 18, '_slider_1_slider_image', 'field_5a9fb5845d1f0'),
(58, 18, 'slider_1_slider_content', 'Never Stop Exploring'),
(59, 18, '_slider_1_slider_content', 'field_5a9fb5ab5d1f1'),
(60, 18, 'slider_2_slider_image', '15'),
(61, 18, '_slider_2_slider_image', 'field_5a9fb5845d1f0'),
(62, 18, 'slider_2_slider_content', 'Never Stop Exploring'),
(63, 18, '_slider_2_slider_content', 'field_5a9fb5ab5d1f1'),
(64, 18, 'slider_3_slider_image', '16'),
(65, 18, '_slider_3_slider_image', 'field_5a9fb5845d1f0'),
(66, 18, 'slider_3_slider_content', 'Never Stop Exploring'),
(67, 18, '_slider_3_slider_content', 'field_5a9fb5ab5d1f1'),
(68, 18, 'slider_4_slider_image', '17'),
(69, 18, '_slider_4_slider_image', 'field_5a9fb5845d1f0'),
(70, 18, 'slider_4_slider_content', 'Never Stop Exploring'),
(71, 18, '_slider_4_slider_content', 'field_5a9fb5ab5d1f1'),
(72, 18, 'slider', '5'),
(73, 18, '_slider', 'field_5a9fb5405d1ef'),
(74, 20, 'slider_0_slider_image', '13'),
(75, 20, '_slider_0_slider_image', 'field_5a9fb5845d1f0'),
(76, 20, 'slider_0_slider_content', 'Never Stop Exploring'),
(77, 20, '_slider_0_slider_content', 'field_5a9fb5ab5d1f1'),
(78, 20, 'slider_1_slider_image', '14'),
(79, 20, '_slider_1_slider_image', 'field_5a9fb5845d1f0'),
(80, 20, 'slider_1_slider_content', 'Never Stop Exploring'),
(81, 20, '_slider_1_slider_content', 'field_5a9fb5ab5d1f1'),
(82, 20, 'slider_2_slider_image', '15'),
(83, 20, '_slider_2_slider_image', 'field_5a9fb5845d1f0'),
(84, 20, 'slider_2_slider_content', 'Never Stop Exploring'),
(85, 20, '_slider_2_slider_content', 'field_5a9fb5ab5d1f1'),
(86, 20, 'slider_3_slider_image', '16'),
(87, 20, '_slider_3_slider_image', 'field_5a9fb5845d1f0'),
(88, 20, 'slider_3_slider_content', 'Never Stop Exploring'),
(89, 20, '_slider_3_slider_content', 'field_5a9fb5ab5d1f1'),
(90, 20, 'slider_4_slider_image', '17'),
(91, 20, '_slider_4_slider_image', 'field_5a9fb5845d1f0'),
(92, 20, 'slider_4_slider_content', 'Never Stop Exploring'),
(93, 20, '_slider_4_slider_content', 'field_5a9fb5ab5d1f1'),
(94, 20, 'slider', '5'),
(95, 20, '_slider', 'field_5a9fb5405d1ef'),
(96, 21, '_wp_attached_file', '2018/03/cyclist.png'),
(97, 21, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1700;s:6:"height";i:500;s:4:"file";s:19:"2018/03/cyclist.png";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"cyclist-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:18:"cyclist-300x88.png";s:5:"width";i:300;s:6:"height";i:88;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:19:"cyclist-768x226.png";s:5:"width";i:768;s:6:"height";i:226;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:20:"cyclist-1024x301.png";s:5:"width";i:1024;s:6:"height";i:301;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(98, 9, '_thumbnail_id', '21'),
(99, 25, '_wp_attached_file', '2018/03/cycle1.png'),
(100, 25, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:500;s:6:"height";i:300;s:4:"file";s:18:"2018/03/cycle1.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"cycle1-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:18:"cycle1-300x180.png";s:5:"width";i:300;s:6:"height";i:180;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(101, 9, 'below_biking_adventure_0_images', '25'),
(102, 9, '_below_biking_adventure_0_images', 'field_5a9fc0fc49888'),
(103, 9, 'below_biking_adventure_0_image_text', 'cx 5 - decal kit 001'),
(104, 9, '_below_biking_adventure_0_image_text', 'field_5a9fc1a104d48'),
(113, 9, 'below_biking_adventure', '3'),
(114, 9, '_below_biking_adventure', 'field_5a9fbfdc42ea5'),
(115, 26, 'slider_0_slider_image', '13'),
(116, 26, '_slider_0_slider_image', 'field_5a9fb5845d1f0'),
(117, 26, 'slider_0_slider_content', 'Never Stop Exploring'),
(118, 26, '_slider_0_slider_content', 'field_5a9fb5ab5d1f1'),
(119, 26, 'slider_1_slider_image', '14'),
(120, 26, '_slider_1_slider_image', 'field_5a9fb5845d1f0'),
(121, 26, 'slider_1_slider_content', 'Never Stop Exploring'),
(122, 26, '_slider_1_slider_content', 'field_5a9fb5ab5d1f1'),
(123, 26, 'slider_2_slider_image', '15'),
(124, 26, '_slider_2_slider_image', 'field_5a9fb5845d1f0'),
(125, 26, 'slider_2_slider_content', 'Never Stop Exploring'),
(126, 26, '_slider_2_slider_content', 'field_5a9fb5ab5d1f1'),
(127, 26, 'slider_3_slider_image', '16'),
(128, 26, '_slider_3_slider_image', 'field_5a9fb5845d1f0'),
(129, 26, 'slider_3_slider_content', 'Never Stop Exploring'),
(130, 26, '_slider_3_slider_content', 'field_5a9fb5ab5d1f1'),
(131, 26, 'slider_4_slider_image', '17'),
(132, 26, '_slider_4_slider_image', 'field_5a9fb5845d1f0'),
(133, 26, 'slider_4_slider_content', 'Never Stop Exploring'),
(134, 26, '_slider_4_slider_content', 'field_5a9fb5ab5d1f1'),
(135, 26, 'slider', '5'),
(136, 26, '_slider', 'field_5a9fb5405d1ef'),
(137, 26, 'below_biking_adventure_0_images', '25'),
(138, 26, '_below_biking_adventure_0_images', 'field_5a9fc0fc49888'),
(139, 26, 'below_biking_adventure_0_image_text', 'cx 5 - decal kit 001'),
(140, 26, '_below_biking_adventure_0_image_text', 'field_5a9fc1a104d48'),
(141, 26, 'below_biking_adventure_1_images', '25'),
(142, 26, '_below_biking_adventure_1_images', 'field_5a9fc0fc49888'),
(143, 26, 'below_biking_adventure_1_image_text', 'cx 5 - decal kit 001'),
(144, 26, '_below_biking_adventure_1_image_text', 'field_5a9fc1a104d48'),
(145, 26, 'below_biking_adventure_2_images', '25'),
(146, 26, '_below_biking_adventure_2_images', 'field_5a9fc0fc49888'),
(147, 26, 'below_biking_adventure_2_image_text', 'cx 5 - decal kit 001'),
(148, 26, '_below_biking_adventure_2_image_text', 'field_5a9fc1a104d48'),
(149, 26, 'below_biking_adventure', '3'),
(150, 26, '_below_biking_adventure', 'field_5a9fbfdc42ea5'),
(151, 27, 'slider_0_slider_image', '13'),
(152, 27, '_slider_0_slider_image', 'field_5a9fb5845d1f0'),
(153, 27, 'slider_0_slider_content', 'Never Stop Exploring'),
(154, 27, '_slider_0_slider_content', 'field_5a9fb5ab5d1f1'),
(155, 27, 'slider_1_slider_image', '14'),
(156, 27, '_slider_1_slider_image', 'field_5a9fb5845d1f0'),
(157, 27, 'slider_1_slider_content', 'Never Stop Exploring'),
(158, 27, '_slider_1_slider_content', 'field_5a9fb5ab5d1f1'),
(159, 27, 'slider_2_slider_image', '15'),
(160, 27, '_slider_2_slider_image', 'field_5a9fb5845d1f0'),
(161, 27, 'slider_2_slider_content', 'Never Stop Exploring'),
(162, 27, '_slider_2_slider_content', 'field_5a9fb5ab5d1f1'),
(163, 27, 'slider_3_slider_image', '16'),
(164, 27, '_slider_3_slider_image', 'field_5a9fb5845d1f0'),
(165, 27, 'slider_3_slider_content', 'Never Stop Exploring'),
(166, 27, '_slider_3_slider_content', 'field_5a9fb5ab5d1f1'),
(167, 27, 'slider_4_slider_image', '17'),
(168, 27, '_slider_4_slider_image', 'field_5a9fb5845d1f0'),
(169, 27, 'slider_4_slider_content', 'Never Stop Exploring'),
(170, 27, '_slider_4_slider_content', 'field_5a9fb5ab5d1f1'),
(171, 27, 'slider', '5'),
(172, 27, '_slider', 'field_5a9fb5405d1ef'),
(173, 27, 'below_biking_adventure_0_images', '25'),
(174, 27, '_below_biking_adventure_0_images', 'field_5a9fc0fc49888'),
(175, 27, 'below_biking_adventure_0_image_text', 'cx 5 - decal kit 001'),
(176, 27, '_below_biking_adventure_0_image_text', 'field_5a9fc1a104d48'),
(177, 27, 'below_biking_adventure', '1'),
(178, 27, '_below_biking_adventure', 'field_5a9fbfdc42ea5'),
(179, 9, 'below_biking_adventure_1_images', '25'),
(180, 9, '_below_biking_adventure_1_images', 'field_5a9fc0fc49888'),
(181, 9, 'below_biking_adventure_1_image_text', 'cx 5 - decal kit 001'),
(182, 9, '_below_biking_adventure_1_image_text', 'field_5a9fc1a104d48'),
(183, 28, 'slider_0_slider_image', '13'),
(184, 28, '_slider_0_slider_image', 'field_5a9fb5845d1f0'),
(185, 28, 'slider_0_slider_content', 'Never Stop Exploring'),
(186, 28, '_slider_0_slider_content', 'field_5a9fb5ab5d1f1'),
(187, 28, 'slider_1_slider_image', '14'),
(188, 28, '_slider_1_slider_image', 'field_5a9fb5845d1f0'),
(189, 28, 'slider_1_slider_content', 'Never Stop Exploring'),
(190, 28, '_slider_1_slider_content', 'field_5a9fb5ab5d1f1'),
(191, 28, 'slider_2_slider_image', '15'),
(192, 28, '_slider_2_slider_image', 'field_5a9fb5845d1f0'),
(193, 28, 'slider_2_slider_content', 'Never Stop Exploring'),
(194, 28, '_slider_2_slider_content', 'field_5a9fb5ab5d1f1'),
(195, 28, 'slider_3_slider_image', '16'),
(196, 28, '_slider_3_slider_image', 'field_5a9fb5845d1f0'),
(197, 28, 'slider_3_slider_content', 'Never Stop Exploring'),
(198, 28, '_slider_3_slider_content', 'field_5a9fb5ab5d1f1'),
(199, 28, 'slider_4_slider_image', '17'),
(200, 28, '_slider_4_slider_image', 'field_5a9fb5845d1f0'),
(201, 28, 'slider_4_slider_content', 'Never Stop Exploring'),
(202, 28, '_slider_4_slider_content', 'field_5a9fb5ab5d1f1'),
(203, 28, 'slider', '5'),
(204, 28, '_slider', 'field_5a9fb5405d1ef'),
(205, 28, 'below_biking_adventure_0_images', '25'),
(206, 28, '_below_biking_adventure_0_images', 'field_5a9fc0fc49888'),
(207, 28, 'below_biking_adventure_0_image_text', 'cx 5 - decal kit 001'),
(208, 28, '_below_biking_adventure_0_image_text', 'field_5a9fc1a104d48'),
(209, 28, 'below_biking_adventure', '2'),
(210, 28, '_below_biking_adventure', 'field_5a9fbfdc42ea5'),
(211, 28, 'below_biking_adventure_1_images', '25'),
(212, 28, '_below_biking_adventure_1_images', 'field_5a9fc0fc49888'),
(213, 28, 'below_biking_adventure_1_image_text', ''),
(214, 28, '_below_biking_adventure_1_image_text', 'field_5a9fc1a104d48'),
(215, 30, '_edit_last', '1'),
(216, 30, '_edit_lock', '1520423435:1'),
(217, 33, '_wp_attached_file', '2018/03/logo2.png'),
(218, 33, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:227;s:6:"height";i:227;s:4:"file";s:17:"2018/03/logo2.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"logo2-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(219, 34, '_wp_attached_file', '2018/03/footer-logo.png'),
(220, 34, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:227;s:6:"height";i:227;s:4:"file";s:23:"2018/03/footer-logo.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"footer-logo-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(221, 35, '_edit_last', '1'),
(222, 35, '_edit_lock', '1520423656:1'),
(223, 46, '_wp_attached_file', '2018/03/youtube.jpg'),
(224, 46, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:595;s:6:"height";i:320;s:4:"file";s:19:"2018/03/youtube.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"youtube-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"youtube-300x161.jpg";s:5:"width";i:300;s:6:"height";i:161;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(225, 9, 'video_link', 'https://www.youtube.com/embed/O3nn5b6G3Js?rel=0&controls=0&showinfo=0'),
(226, 9, '_video_link', 'field_5a9fd50560035'),
(227, 9, 'video_code', 'O3nn5b6G3Js'),
(228, 9, '_video_code', 'field_5a9fd5222acca'),
(229, 9, 'video_image', '46'),
(230, 9, '_video_image', 'field_5a9fd55eb1e7d'),
(231, 47, 'slider_0_slider_image', '13'),
(232, 47, '_slider_0_slider_image', 'field_5a9fb5845d1f0'),
(233, 47, 'slider_0_slider_content', 'Never Stop Exploring'),
(234, 47, '_slider_0_slider_content', 'field_5a9fb5ab5d1f1'),
(235, 47, 'slider_1_slider_image', '14'),
(236, 47, '_slider_1_slider_image', 'field_5a9fb5845d1f0'),
(237, 47, 'slider_1_slider_content', 'Never Stop Exploring'),
(238, 47, '_slider_1_slider_content', 'field_5a9fb5ab5d1f1'),
(239, 47, 'slider_2_slider_image', '15'),
(240, 47, '_slider_2_slider_image', 'field_5a9fb5845d1f0'),
(241, 47, 'slider_2_slider_content', 'Never Stop Exploring'),
(242, 47, '_slider_2_slider_content', 'field_5a9fb5ab5d1f1'),
(243, 47, 'slider_3_slider_image', '16'),
(244, 47, '_slider_3_slider_image', 'field_5a9fb5845d1f0'),
(245, 47, 'slider_3_slider_content', 'Never Stop Exploring'),
(246, 47, '_slider_3_slider_content', 'field_5a9fb5ab5d1f1'),
(247, 47, 'slider_4_slider_image', '17'),
(248, 47, '_slider_4_slider_image', 'field_5a9fb5845d1f0'),
(249, 47, 'slider_4_slider_content', 'Never Stop Exploring'),
(250, 47, '_slider_4_slider_content', 'field_5a9fb5ab5d1f1'),
(251, 47, 'slider', '5'),
(252, 47, '_slider', 'field_5a9fb5405d1ef'),
(253, 47, 'below_biking_adventure_0_images', '25'),
(254, 47, '_below_biking_adventure_0_images', 'field_5a9fc0fc49888'),
(255, 47, 'below_biking_adventure_0_image_text', 'cx 5 - decal kit 001'),
(256, 47, '_below_biking_adventure_0_image_text', 'field_5a9fc1a104d48'),
(257, 47, 'below_biking_adventure', '2'),
(258, 47, '_below_biking_adventure', 'field_5a9fbfdc42ea5'),
(259, 47, 'below_biking_adventure_1_images', '25'),
(260, 47, '_below_biking_adventure_1_images', 'field_5a9fc0fc49888'),
(261, 47, 'below_biking_adventure_1_image_text', ''),
(262, 47, '_below_biking_adventure_1_image_text', 'field_5a9fc1a104d48'),
(263, 47, 'video_link', 'https://www.youtube.com/embed/O3nn5b6G3Js?rel=0&amp;controls=0&amp;showinfo=0'),
(264, 47, '_video_link', 'field_5a9fd50560035'),
(265, 47, 'video_code', 'O3nn5b6G3Js'),
(266, 47, '_video_code', 'field_5a9fd5222acca'),
(267, 47, 'video_image', '46'),
(268, 47, '_video_image', 'field_5a9fd55eb1e7d'),
(269, 9, 'journey_stories', '<h2>Experiences Journey Stories</h2>\r\nCheck out the brand new Bicycle Network merchandise range! Our winter collection is an affordable way to keep warm and helps you wear your support on your sleeve to spread the message of bike advocacy when you’re out for a spin.'),
(270, 9, '_journey_stories', 'field_5a9fd736baff4'),
(271, 49, 'slider_0_slider_image', '13'),
(272, 49, '_slider_0_slider_image', 'field_5a9fb5845d1f0'),
(273, 49, 'slider_0_slider_content', 'Never Stop Exploring'),
(274, 49, '_slider_0_slider_content', 'field_5a9fb5ab5d1f1'),
(275, 49, 'slider_1_slider_image', '14'),
(276, 49, '_slider_1_slider_image', 'field_5a9fb5845d1f0'),
(277, 49, 'slider_1_slider_content', 'Never Stop Exploring'),
(278, 49, '_slider_1_slider_content', 'field_5a9fb5ab5d1f1'),
(279, 49, 'slider_2_slider_image', '15'),
(280, 49, '_slider_2_slider_image', 'field_5a9fb5845d1f0'),
(281, 49, 'slider_2_slider_content', 'Never Stop Exploring'),
(282, 49, '_slider_2_slider_content', 'field_5a9fb5ab5d1f1'),
(283, 49, 'slider_3_slider_image', '16'),
(284, 49, '_slider_3_slider_image', 'field_5a9fb5845d1f0'),
(285, 49, 'slider_3_slider_content', 'Never Stop Exploring'),
(286, 49, '_slider_3_slider_content', 'field_5a9fb5ab5d1f1'),
(287, 49, 'slider_4_slider_image', '17'),
(288, 49, '_slider_4_slider_image', 'field_5a9fb5845d1f0'),
(289, 49, 'slider_4_slider_content', 'Never Stop Exploring'),
(290, 49, '_slider_4_slider_content', 'field_5a9fb5ab5d1f1'),
(291, 49, 'slider', '5'),
(292, 49, '_slider', 'field_5a9fb5405d1ef'),
(293, 49, 'below_biking_adventure_0_images', '25'),
(294, 49, '_below_biking_adventure_0_images', 'field_5a9fc0fc49888'),
(295, 49, 'below_biking_adventure_0_image_text', 'cx 5 - decal kit 001'),
(296, 49, '_below_biking_adventure_0_image_text', 'field_5a9fc1a104d48'),
(297, 49, 'below_biking_adventure', '2'),
(298, 49, '_below_biking_adventure', 'field_5a9fbfdc42ea5'),
(299, 49, 'below_biking_adventure_1_images', '25'),
(300, 49, '_below_biking_adventure_1_images', 'field_5a9fc0fc49888'),
(301, 49, 'below_biking_adventure_1_image_text', ''),
(302, 49, '_below_biking_adventure_1_image_text', 'field_5a9fc1a104d48'),
(303, 49, 'video_link', 'https://www.youtube.com/embed/O3nn5b6G3Js?rel=0&controls=0&showinfo=0'),
(304, 49, '_video_link', 'field_5a9fd50560035'),
(305, 49, 'video_code', 'O3nn5b6G3Js'),
(306, 49, '_video_code', 'field_5a9fd5222acca'),
(307, 49, 'video_image', '46'),
(308, 49, '_video_image', 'field_5a9fd55eb1e7d'),
(309, 49, 'journey_stories', '<h2>Experiences Journey Stories</h2>\r\nCheck out the brand new Bicycle Network merchandise range! Our winter collection is an affordable way to keep warm and helps you wear your support on your sleeve to spread the message of bike advocacy when you’re out for a spin.'),
(310, 49, '_journey_stories', 'field_5a9fd736baff4'),
(311, 50, '_edit_last', '1'),
(312, 50, '_edit_lock', '1520489719:1'),
(313, 52, '_edit_last', '1'),
(314, 52, '_edit_lock', '1520496702:1'),
(315, 55, '_edit_last', '1'),
(316, 55, '_edit_lock', '1520497334:1'),
(317, 57, '_edit_last', '1'),
(318, 57, '_edit_lock', '1520424967:1'),
(319, 59, '_edit_last', '1'),
(320, 59, '_edit_lock', '1520497926:1'),
(321, 61, '_menu_item_type', 'post_type'),
(322, 61, '_menu_item_menu_item_parent', '0'),
(323, 61, '_menu_item_object_id', '50'),
(324, 61, '_menu_item_object', 'page'),
(325, 61, '_menu_item_target', ''),
(326, 61, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(327, 61, '_menu_item_xfn', ''),
(328, 61, '_menu_item_url', ''),
(330, 62, '_menu_item_type', 'post_type'),
(331, 62, '_menu_item_menu_item_parent', '0'),
(332, 62, '_menu_item_object_id', '52'),
(333, 62, '_menu_item_object', 'page'),
(334, 62, '_menu_item_target', ''),
(335, 62, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(336, 62, '_menu_item_xfn', ''),
(337, 62, '_menu_item_url', ''),
(339, 63, '_menu_item_type', 'post_type'),
(340, 63, '_menu_item_menu_item_parent', '0'),
(341, 63, '_menu_item_object_id', '57'),
(342, 63, '_menu_item_object', 'page'),
(343, 63, '_menu_item_target', ''),
(344, 63, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(345, 63, '_menu_item_xfn', ''),
(346, 63, '_menu_item_url', ''),
(348, 64, '_menu_item_type', 'post_type'),
(349, 64, '_menu_item_menu_item_parent', '0'),
(350, 64, '_menu_item_object_id', '55'),
(351, 64, '_menu_item_object', 'page'),
(352, 64, '_menu_item_target', ''),
(353, 64, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(354, 64, '_menu_item_xfn', ''),
(355, 64, '_menu_item_url', ''),
(357, 65, '_menu_item_type', 'post_type'),
(358, 65, '_menu_item_menu_item_parent', '0'),
(359, 65, '_menu_item_object_id', '59'),
(360, 65, '_menu_item_object', 'page'),
(361, 65, '_menu_item_target', ''),
(362, 65, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(363, 65, '_menu_item_xfn', ''),
(364, 65, '_menu_item_url', ''),
(366, 67, '_wp_attached_file', '2018/03/slide1.jpg'),
(367, 67, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:674;s:6:"height";i:700;s:4:"file";s:18:"2018/03/slide1.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"slide1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:18:"slide1-289x300.jpg";s:5:"width";i:289;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(368, 9, 'sidebar_image_below_biking_adventure', '67'),
(369, 9, '_sidebar_image_below_biking_adventure', 'field_5a9fda61bc8bc'),
(370, 68, 'slider_0_slider_image', '13'),
(371, 68, '_slider_0_slider_image', 'field_5a9fb5845d1f0'),
(372, 68, 'slider_0_slider_content', 'Never Stop Exploring'),
(373, 68, '_slider_0_slider_content', 'field_5a9fb5ab5d1f1'),
(374, 68, 'slider_1_slider_image', '14'),
(375, 68, '_slider_1_slider_image', 'field_5a9fb5845d1f0'),
(376, 68, 'slider_1_slider_content', 'Never Stop Exploring'),
(377, 68, '_slider_1_slider_content', 'field_5a9fb5ab5d1f1'),
(378, 68, 'slider_2_slider_image', '15'),
(379, 68, '_slider_2_slider_image', 'field_5a9fb5845d1f0'),
(380, 68, 'slider_2_slider_content', 'Never Stop Exploring'),
(381, 68, '_slider_2_slider_content', 'field_5a9fb5ab5d1f1'),
(382, 68, 'slider_3_slider_image', '16'),
(383, 68, '_slider_3_slider_image', 'field_5a9fb5845d1f0'),
(384, 68, 'slider_3_slider_content', 'Never Stop Exploring'),
(385, 68, '_slider_3_slider_content', 'field_5a9fb5ab5d1f1'),
(386, 68, 'slider_4_slider_image', '17'),
(387, 68, '_slider_4_slider_image', 'field_5a9fb5845d1f0'),
(388, 68, 'slider_4_slider_content', 'Never Stop Exploring'),
(389, 68, '_slider_4_slider_content', 'field_5a9fb5ab5d1f1'),
(390, 68, 'slider', '5'),
(391, 68, '_slider', 'field_5a9fb5405d1ef'),
(392, 68, 'below_biking_adventure_0_images', '25'),
(393, 68, '_below_biking_adventure_0_images', 'field_5a9fc0fc49888'),
(394, 68, 'below_biking_adventure_0_image_text', 'cx 5 - decal kit 001'),
(395, 68, '_below_biking_adventure_0_image_text', 'field_5a9fc1a104d48'),
(396, 68, 'below_biking_adventure', '2'),
(397, 68, '_below_biking_adventure', 'field_5a9fbfdc42ea5'),
(398, 68, 'below_biking_adventure_1_images', '25'),
(399, 68, '_below_biking_adventure_1_images', 'field_5a9fc0fc49888'),
(400, 68, 'below_biking_adventure_1_image_text', ''),
(401, 68, '_below_biking_adventure_1_image_text', 'field_5a9fc1a104d48'),
(402, 68, 'video_link', 'https://www.youtube.com/embed/O3nn5b6G3Js?rel=0&controls=0&showinfo=0'),
(403, 68, '_video_link', 'field_5a9fd50560035'),
(404, 68, 'video_code', 'O3nn5b6G3Js'),
(405, 68, '_video_code', 'field_5a9fd5222acca'),
(406, 68, 'video_image', '46'),
(407, 68, '_video_image', 'field_5a9fd55eb1e7d'),
(408, 68, 'journey_stories', '<h2>Experiences Journey Stories</h2>\r\nCheck out the brand new Bicycle Network merchandise range! Our winter collection is an affordable way to keep warm and helps you wear your support on your sleeve to spread the message of bike advocacy when you’re out for a spin.'),
(409, 68, '_journey_stories', 'field_5a9fd736baff4'),
(410, 68, 'sidebar_image_below_biking_adventure', '67'),
(411, 68, '_sidebar_image_below_biking_adventure', 'field_5a9fda61bc8bc'),
(412, 9, 'below_biking_adventure_2_images', '25'),
(413, 9, '_below_biking_adventure_2_images', 'field_5a9fc0fc49888'),
(414, 9, 'below_biking_adventure_2_image_text', 'cx 5 - decal kit 001'),
(415, 9, '_below_biking_adventure_2_image_text', 'field_5a9fc1a104d48'),
(416, 69, 'slider_0_slider_image', '13'),
(417, 69, '_slider_0_slider_image', 'field_5a9fb5845d1f0'),
(418, 69, 'slider_0_slider_content', 'Never Stop Exploring'),
(419, 69, '_slider_0_slider_content', 'field_5a9fb5ab5d1f1'),
(420, 69, 'slider_1_slider_image', '14'),
(421, 69, '_slider_1_slider_image', 'field_5a9fb5845d1f0'),
(422, 69, 'slider_1_slider_content', 'Never Stop Exploring'),
(423, 69, '_slider_1_slider_content', 'field_5a9fb5ab5d1f1'),
(424, 69, 'slider_2_slider_image', '15'),
(425, 69, '_slider_2_slider_image', 'field_5a9fb5845d1f0'),
(426, 69, 'slider_2_slider_content', 'Never Stop Exploring'),
(427, 69, '_slider_2_slider_content', 'field_5a9fb5ab5d1f1'),
(428, 69, 'slider_3_slider_image', '16'),
(429, 69, '_slider_3_slider_image', 'field_5a9fb5845d1f0'),
(430, 69, 'slider_3_slider_content', 'Never Stop Exploring'),
(431, 69, '_slider_3_slider_content', 'field_5a9fb5ab5d1f1'),
(432, 69, 'slider_4_slider_image', '17'),
(433, 69, '_slider_4_slider_image', 'field_5a9fb5845d1f0'),
(434, 69, 'slider_4_slider_content', 'Never Stop Exploring'),
(435, 69, '_slider_4_slider_content', 'field_5a9fb5ab5d1f1'),
(436, 69, 'slider', '5'),
(437, 69, '_slider', 'field_5a9fb5405d1ef'),
(438, 69, 'below_biking_adventure_0_images', '25'),
(439, 69, '_below_biking_adventure_0_images', 'field_5a9fc0fc49888'),
(440, 69, 'below_biking_adventure_0_image_text', 'cx 5 - decal kit 001'),
(441, 69, '_below_biking_adventure_0_image_text', 'field_5a9fc1a104d48'),
(442, 69, 'below_biking_adventure', '3'),
(443, 69, '_below_biking_adventure', 'field_5a9fbfdc42ea5'),
(444, 69, 'below_biking_adventure_1_images', '25'),
(445, 69, '_below_biking_adventure_1_images', 'field_5a9fc0fc49888'),
(446, 69, 'below_biking_adventure_1_image_text', 'cx 5 - decal kit 001'),
(447, 69, '_below_biking_adventure_1_image_text', 'field_5a9fc1a104d48'),
(448, 69, 'video_link', 'https://www.youtube.com/embed/O3nn5b6G3Js?rel=0&controls=0&showinfo=0'),
(449, 69, '_video_link', 'field_5a9fd50560035'),
(450, 69, 'video_code', 'O3nn5b6G3Js'),
(451, 69, '_video_code', 'field_5a9fd5222acca'),
(452, 69, 'video_image', '46'),
(453, 69, '_video_image', 'field_5a9fd55eb1e7d'),
(454, 69, 'journey_stories', '<h2>Experiences Journey Stories</h2>\r\nCheck out the brand new Bicycle Network merchandise range! Our winter collection is an affordable way to keep warm and helps you wear your support on your sleeve to spread the message of bike advocacy when you’re out for a spin.'),
(455, 69, '_journey_stories', 'field_5a9fd736baff4'),
(456, 69, 'sidebar_image_below_biking_adventure', '67'),
(457, 69, '_sidebar_image_below_biking_adventure', 'field_5a9fda61bc8bc'),
(458, 69, 'below_biking_adventure_2_images', '25'),
(459, 69, '_below_biking_adventure_2_images', 'field_5a9fc0fc49888'),
(460, 69, 'below_biking_adventure_2_image_text', 'cx 5 - decal kit 001'),
(461, 69, '_below_biking_adventure_2_image_text', 'field_5a9fc1a104d48'),
(462, 50, '_wp_page_template', 'templates/about-us.php'),
(463, 70, '_wp_attached_file', '2018/03/about-bg.jpg'),
(464, 70, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:6000;s:6:"height";i:3000;s:4:"file";s:20:"2018/03/about-bg.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"about-bg-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"about-bg-300x150.jpg";s:5:"width";i:300;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"about-bg-768x384.jpg";s:5:"width";i:768;s:6:"height";i:384;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"about-bg-1024x512.jpg";s:5:"width";i:1024;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:18:"Naveen Photography";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(465, 50, '_thumbnail_id', '70'),
(466, 73, '_edit_last', '1'),
(467, 73, '_edit_lock', '1520490361:1'),
(468, 75, '_wp_attached_file', '2018/03/about-side.jpg'),
(469, 75, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:4000;s:6:"height";i:5000;s:4:"file";s:22:"2018/03/about-side.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:22:"about-side-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:22:"about-side-240x300.jpg";s:5:"width";i:240;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:22:"about-side-768x960.jpg";s:5:"width";i:768;s:6:"height";i:960;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:23:"about-side-819x1024.jpg";s:5:"width";i:819;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:18:"Naveen Photography";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(470, 73, '_wp_trash_meta_status', 'publish'),
(471, 73, '_wp_trash_meta_time', '1520490511'),
(472, 73, '_wp_desired_post_slug', 'group_5aa0d592c6848'),
(473, 76, '_edit_last', '1'),
(474, 76, '_edit_lock', '1520496159:1'),
(475, 52, '_wp_page_template', 'templates/biking-trial.php'),
(476, 78, '_wp_attached_file', '2018/03/intro-bg.jpg'),
(477, 78, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1920;s:6:"height";i:1000;s:4:"file";s:20:"2018/03/intro-bg.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"intro-bg-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"intro-bg-300x156.jpg";s:5:"width";i:300;s:6:"height";i:156;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"intro-bg-768x400.jpg";s:5:"width";i:768;s:6:"height";i:400;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"intro-bg-1024x533.jpg";s:5:"width";i:1024;s:6:"height";i:533;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(478, 52, '_thumbnail_id', '78'),
(479, 52, 'highlight_content', '<ul>\r\n 	<li>Distance: Roughly 12 km (3 hrs ride)</li>\r\n 	<li>Point of Interest: 360 view tower + Serene Pine forests + Village bike ride with stunning mountain views</li>\r\n 	<li>Level: Beginner to medium</li>\r\n 	<li>Suitable for all ages and abilities</li>\r\n 	<li>This self-guided tour is flexible and can be customized to your group’s preferences</li>\r\n</ul>'),
(480, 52, '_highlight_content', 'field_5aa0de851055f'),
(481, 52, 'detail_content', 'Your tour starts at Nagarkot Himalayan biking adventure shop and first takes you through an uphill view tower, a great spot with excellent mountain views (weather permitted).\r\n\r\nNext, a pleasant downhill track through pine forests and then through the villages before reaching the finishing point.\r\n\r\nIf with No Guide, You will be given a map and a written detailed itinerary to follow at your own pace, enjoy the route and feel free to stop and stay at the various points of interest.'),
(482, 52, '_detail_content', 'field_5aa0e00266336'),
(483, 52, 'with_guide_content', 'Our guides are all highly qualified and expert riders. They know all the best trails and ways around the foothills. Your guides are also all bike mechanics and can fix any problems that should occur on the trail. Your guides will be trained in first aid and emergency procedure. Most importantly your guides are all locals, being well versed in the local culture and history to allow you to get the most out of your trip. They are fun and friendly for your upmost enjoyment.'),
(484, 52, '_with_guide_content', 'field_5aa0e06366338'),
(485, 86, 'highlight_content', '<ul>\r\n 	<li>Distance: Roughly 12 km (3 hrs ride)</li>\r\n 	<li>Point of Interest: 360 view tower + Serene Pine forests + Village bike ride with stunning mountain views</li>\r\n 	<li>Level: Beginner to medium</li>\r\n 	<li>Suitable for all ages and abilities</li>\r\n 	<li>This self-guided tour is flexible and can be customized to your group’s preferences</li>\r\n</ul>'),
(486, 86, '_highlight_content', 'field_5aa0de851055f'),
(487, 86, 'detail_content', 'Your tour starts at Nagarkot Himalayan biking adventure shop and first takes you through an uphill view tower, a great spot with excellent mountain views (weather permitted).\r\n\r\nNext, a pleasant downhill track through pine forests and then through the villages before reaching the finishing point.\r\n\r\nIf with No Guide, You will be given a map and a written detailed itinerary to follow at your own pace, enjoy the route and feel free to stop and stay at the various points of interest.'),
(488, 86, '_detail_content', 'field_5aa0e00266336'),
(489, 86, 'with_guide_content', 'Our guides are all highly qualified and expert riders. They know all the best trails and ways around the foothills. Your guides are also all bike mechanics and can fix any problems that should occur on the trail. Your guides will be trained in first aid and emergency procedure. Most importantly your guides are all locals, being well versed in the local culture and history to allow you to get the most out of your trip. They are fun and friendly for your upmost enjoyment.'),
(490, 86, '_with_guide_content', 'field_5aa0e06366338'),
(491, 87, '_form', '<div class="row">\n   <div class="col-sm-12 form-group">\n    [select* noofpax id:noofrooms class:form-control "--Number Of People--" "1" "2" "3" "3+"]\n</div>\n\n<div class="col-sm-12 form-group">                 \n[text* name id:name class:form-control placeholder "Name"]\n</div>\n\n<div class="col-sm-12 form-group">\n[email* email id:email class:form-control placeholder "Email"]\n</div>\n\n<div class="col-sm-12 form-group">\n[number* contact_number id:contact_number class:form-control placeholder "Number"]\n</div>\n\n<div class="col-sm-12 form-group">\n[select* country id:country class:form-control "Select Your Country" "Australia" "Bhutan" "China " "India" "Japan" "USA" "Canada" "Bangladesh" "France" "UK" "Portugal" "Nepal" "Netherlands" "NewZealand" "Norway" "Pakistan" "Poland"]\n                 \n</div>\n \n<div class="clearfix"></div>\n                  \n <div class="col-sm-12 form-group">\n [submit id:contact_submit class:btn class:send-us class:hvr-fade "Submit"]\n </div>\n </div>'),
(492, 87, '_mail', 'a:9:{s:6:"active";b:1;s:7:"subject";s:32:"Himalaya Biking "[your-subject]"";s:6:"sender";s:41:"[your-name] <pracriti11neupane@gmail.com>";s:9:"recipient";s:27:"pracriti11neupane@gmail.com";s:4:"body";s:194:"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Himalaya Biking (http://localhost/office_projects/cycling)";s:18:"additional_headers";s:22:"Reply-To: [your-email]";s:11:"attachments";s:0:"";s:8:"use_html";b:0;s:13:"exclude_blank";b:0;}'),
(493, 87, '_mail_2', 'a:9:{s:6:"active";b:0;s:7:"subject";s:32:"Himalaya Biking "[your-subject]"";s:6:"sender";s:45:"Himalaya Biking <pracriti11neupane@gmail.com>";s:9:"recipient";s:12:"[your-email]";s:4:"body";s:136:"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Himalaya Biking (http://localhost/office_projects/cycling)";s:18:"additional_headers";s:37:"Reply-To: pracriti11neupane@gmail.com";s:11:"attachments";s:0:"";s:8:"use_html";b:0;s:13:"exclude_blank";b:0;}'),
(494, 87, '_messages', 'a:23:{s:12:"mail_sent_ok";s:45:"Thank you for your message. It has been sent.";s:12:"mail_sent_ng";s:71:"There was an error trying to send your message. Please try again later.";s:16:"validation_error";s:61:"One or more fields have an error. Please check and try again.";s:4:"spam";s:71:"There was an error trying to send your message. Please try again later.";s:12:"accept_terms";s:69:"You must accept the terms and conditions before sending your message.";s:16:"invalid_required";s:22:"The field is required.";s:16:"invalid_too_long";s:22:"The field is too long.";s:17:"invalid_too_short";s:23:"The field is too short.";s:12:"invalid_date";s:29:"The date format is incorrect.";s:14:"date_too_early";s:44:"The date is before the earliest one allowed.";s:13:"date_too_late";s:41:"The date is after the latest one allowed.";s:13:"upload_failed";s:46:"There was an unknown error uploading the file.";s:24:"upload_file_type_invalid";s:49:"You are not allowed to upload files of this type.";s:21:"upload_file_too_large";s:20:"The file is too big.";s:23:"upload_failed_php_error";s:38:"There was an error uploading the file.";s:14:"invalid_number";s:29:"The number format is invalid.";s:16:"number_too_small";s:47:"The number is smaller than the minimum allowed.";s:16:"number_too_large";s:46:"The number is larger than the maximum allowed.";s:23:"quiz_answer_not_correct";s:36:"The answer to the quiz is incorrect.";s:17:"captcha_not_match";s:31:"Your entered code is incorrect.";s:13:"invalid_email";s:38:"The e-mail address entered is invalid.";s:11:"invalid_url";s:19:"The URL is invalid.";s:11:"invalid_tel";s:32:"The telephone number is invalid.";}'),
(495, 87, '_additional_settings', ''),
(496, 87, '_locale', 'en_US'),
(501, 87, '_config_errors', 'a:2:{s:9:"form.body";a:1:{i:0;a:2:{s:4:"code";i:107;s:4:"args";a:3:{s:7:"message";s:55:"Unavailable names (%names%) are used for form controls.";s:6:"params";a:1:{s:5:"names";s:6:""name"";}s:4:"link";s:63:"https://contactform7.com/configuration-errors/unavailable-names";}}}s:23:"mail.additional_headers";a:1:{i:0;a:2:{s:4:"code";i:102;s:4:"args";a:3:{s:7:"message";s:51:"Invalid mailbox syntax is used in the %name% field.";s:6:"params";a:1:{s:4:"name";s:8:"Reply-To";}s:4:"link";s:68:"https://contactform7.com/configuration-errors/invalid-mailbox-syntax";}}}}'),
(502, 89, '_wp_attached_file', '2018/03/banner5-1.jpg'),
(503, 89, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1600;s:6:"height";i:750;s:4:"file";s:21:"2018/03/banner5-1.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:21:"banner5-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:21:"banner5-1-300x141.jpg";s:5:"width";i:300;s:6:"height";i:141;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:21:"banner5-1-768x360.jpg";s:5:"width";i:768;s:6:"height";i:360;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:22:"banner5-1-1024x480.jpg";s:5:"width";i:1024;s:6:"height";i:480;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(504, 52, 'central_part_image', '89'),
(505, 52, '_central_part_image', 'field_5aa0eddad0dca'),
(506, 90, 'highlight_content', '<ul>\r\n 	<li>Distance: Roughly 12 km (3 hrs ride)</li>\r\n 	<li>Point of Interest: 360 view tower + Serene Pine forests + Village bike ride with stunning mountain views</li>\r\n 	<li>Level: Beginner to medium</li>\r\n 	<li>Suitable for all ages and abilities</li>\r\n 	<li>This self-guided tour is flexible and can be customized to your group’s preferences</li>\r\n</ul>'),
(507, 90, '_highlight_content', 'field_5aa0de851055f'),
(508, 90, 'detail_content', 'Your tour starts at Nagarkot Himalayan biking adventure shop and first takes you through an uphill view tower, a great spot with excellent mountain views (weather permitted).\r\n\r\nNext, a pleasant downhill track through pine forests and then through the villages before reaching the finishing point.\r\n\r\nIf with No Guide, You will be given a map and a written detailed itinerary to follow at your own pace, enjoy the route and feel free to stop and stay at the various points of interest.'),
(509, 90, '_detail_content', 'field_5aa0e00266336'),
(510, 90, 'with_guide_content', 'Our guides are all highly qualified and expert riders. They know all the best trails and ways around the foothills. Your guides are also all bike mechanics and can fix any problems that should occur on the trail. Your guides will be trained in first aid and emergency procedure. Most importantly your guides are all locals, being well versed in the local culture and history to allow you to get the most out of your trip. They are fun and friendly for your upmost enjoyment.'),
(511, 90, '_with_guide_content', 'field_5aa0e06366338'),
(512, 90, 'central_part_image', '89'),
(513, 90, '_central_part_image', 'field_5aa0eddad0dca'),
(514, 91, '_edit_last', '1'),
(515, 91, '_edit_lock', '1520497866:1'),
(516, 94, '_wp_attached_file', '2018/03/23.jpeg'),
(517, 94, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:15:"2018/03/23.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:15:"23-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:15:"23-300x200.jpeg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:15:"23-768x512.jpeg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:16:"23-1024x682.jpeg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(518, 95, '_wp_attached_file', '2018/03/22.jpeg'),
(519, 95, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:15:"2018/03/22.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:15:"22-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:15:"22-300x200.jpeg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:15:"22-768x512.jpeg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:16:"22-1024x682.jpeg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(520, 96, '_wp_attached_file', '2018/03/21.jpeg'),
(521, 96, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:15:"2018/03/21.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:15:"21-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:15:"21-300x200.jpeg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:15:"21-768x512.jpeg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:16:"21-1024x682.jpeg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(522, 97, '_wp_attached_file', '2018/03/20.jpeg');
INSERT INTO `web_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(523, 97, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:15:"2018/03/20.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:15:"20-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:15:"20-300x200.jpeg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:15:"20-768x512.jpeg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:16:"20-1024x682.jpeg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(524, 98, '_wp_attached_file', '2018/03/19.jpeg'),
(525, 98, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:15:"2018/03/19.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:15:"19-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:15:"19-300x200.jpeg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:15:"19-768x512.jpeg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:16:"19-1024x682.jpeg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(526, 99, '_wp_attached_file', '2018/03/18.jpeg'),
(527, 99, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:15:"2018/03/18.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:15:"18-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:15:"18-300x200.jpeg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:15:"18-768x512.jpeg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:16:"18-1024x682.jpeg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(528, 100, '_wp_attached_file', '2018/03/17.jpeg'),
(529, 100, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:853;s:6:"height";i:1280;s:4:"file";s:15:"2018/03/17.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:15:"17-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:15:"17-200x300.jpeg";s:5:"width";i:200;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:16:"17-768x1152.jpeg";s:5:"width";i:768;s:6:"height";i:1152;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:16:"17-682x1024.jpeg";s:5:"width";i:682;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(530, 101, '_wp_attached_file', '2018/03/16.jpeg'),
(531, 101, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:15:"2018/03/16.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:15:"16-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:15:"16-300x200.jpeg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:15:"16-768x512.jpeg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:16:"16-1024x682.jpeg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(532, 102, '_wp_attached_file', '2018/03/15.jpeg'),
(533, 102, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:15:"2018/03/15.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:15:"15-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:15:"15-300x200.jpeg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:15:"15-768x512.jpeg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:16:"15-1024x682.jpeg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(534, 103, '_wp_attached_file', '2018/03/14.jpeg'),
(535, 103, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:15:"2018/03/14.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:15:"14-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:15:"14-300x200.jpeg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:15:"14-768x512.jpeg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:16:"14-1024x682.jpeg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(536, 104, '_wp_attached_file', '2018/03/13.jpeg'),
(537, 104, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:15:"2018/03/13.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:15:"13-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:15:"13-300x200.jpeg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:15:"13-768x512.jpeg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:16:"13-1024x682.jpeg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(538, 105, '_wp_attached_file', '2018/03/12.jpeg'),
(539, 105, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:15:"2018/03/12.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:15:"12-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:15:"12-300x200.jpeg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:15:"12-768x512.jpeg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:16:"12-1024x682.jpeg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(540, 106, '_wp_attached_file', '2018/03/11.jpeg'),
(541, 106, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:15:"2018/03/11.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:15:"11-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:15:"11-300x200.jpeg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:15:"11-768x512.jpeg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:16:"11-1024x682.jpeg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(542, 107, '_wp_attached_file', '2018/03/10.jpeg'),
(543, 107, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:15:"2018/03/10.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:15:"10-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:15:"10-300x200.jpeg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:15:"10-768x512.jpeg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:16:"10-1024x682.jpeg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(544, 108, '_wp_attached_file', '2018/03/9.jpeg'),
(545, 108, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:14:"2018/03/9.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:14:"9-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:14:"9-300x200.jpeg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:14:"9-768x512.jpeg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:15:"9-1024x682.jpeg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(546, 109, '_wp_attached_file', '2018/03/8.jpeg'),
(547, 109, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:14:"2018/03/8.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:14:"8-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:14:"8-300x200.jpeg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:14:"8-768x512.jpeg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:15:"8-1024x682.jpeg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(548, 110, '_wp_attached_file', '2018/03/7.jpeg'),
(549, 110, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:14:"2018/03/7.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:14:"7-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:14:"7-300x200.jpeg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:14:"7-768x512.jpeg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:15:"7-1024x682.jpeg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(550, 111, '_wp_attached_file', '2018/03/6.jpg'),
(551, 111, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:6000;s:6:"height";i:4000;s:4:"file";s:13:"2018/03/6.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:13:"6-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:13:"6-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:13:"6-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:14:"6-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:2:"13";s:6:"credit";s:14:"Naveen Bhasima";s:6:"camera";s:11:"NIKON D7200";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1516108471";s:9:"copyright";s:18:"Naveen Photography";s:12:"focal_length";s:2:"40";s:3:"iso";s:3:"125";s:13:"shutter_speed";s:5:"0.008";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(552, 112, '_wp_attached_file', '2018/03/23-1.jpeg'),
(553, 112, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:17:"2018/03/23-1.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"23-1-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:17:"23-1-300x200.jpeg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:17:"23-1-768x512.jpeg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:18:"23-1-1024x682.jpeg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(554, 113, '_wp_attached_file', '2018/03/22-1.jpeg'),
(555, 113, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:17:"2018/03/22-1.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"22-1-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:17:"22-1-300x200.jpeg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:17:"22-1-768x512.jpeg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:18:"22-1-1024x682.jpeg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(556, 114, '_wp_attached_file', '2018/03/21-1.jpeg'),
(557, 114, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:17:"2018/03/21-1.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"21-1-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:17:"21-1-300x200.jpeg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:17:"21-1-768x512.jpeg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:18:"21-1-1024x682.jpeg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(558, 115, '_wp_attached_file', '2018/03/20-1.jpeg'),
(559, 115, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:17:"2018/03/20-1.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"20-1-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:17:"20-1-300x200.jpeg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:17:"20-1-768x512.jpeg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:18:"20-1-1024x682.jpeg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(560, 116, '_wp_attached_file', '2018/03/19-1.jpeg'),
(561, 116, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:17:"2018/03/19-1.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"19-1-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:17:"19-1-300x200.jpeg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:17:"19-1-768x512.jpeg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:18:"19-1-1024x682.jpeg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(562, 117, '_wp_attached_file', '2018/03/18-1.jpeg'),
(563, 117, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:17:"2018/03/18-1.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"18-1-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:17:"18-1-300x200.jpeg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:17:"18-1-768x512.jpeg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:18:"18-1-1024x682.jpeg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(564, 118, '_wp_attached_file', '2018/03/17-1.jpeg'),
(565, 118, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:853;s:6:"height";i:1280;s:4:"file";s:17:"2018/03/17-1.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"17-1-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:17:"17-1-200x300.jpeg";s:5:"width";i:200;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:18:"17-1-768x1152.jpeg";s:5:"width";i:768;s:6:"height";i:1152;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:18:"17-1-682x1024.jpeg";s:5:"width";i:682;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(566, 119, '_wp_attached_file', '2018/03/16-1.jpeg'),
(567, 119, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:17:"2018/03/16-1.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"16-1-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:17:"16-1-300x200.jpeg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:17:"16-1-768x512.jpeg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:18:"16-1-1024x682.jpeg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(568, 120, '_wp_attached_file', '2018/03/15-1.jpeg'),
(569, 120, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:17:"2018/03/15-1.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"15-1-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:17:"15-1-300x200.jpeg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:17:"15-1-768x512.jpeg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:18:"15-1-1024x682.jpeg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(570, 121, '_wp_attached_file', '2018/03/14-1.jpeg'),
(571, 121, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:17:"2018/03/14-1.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"14-1-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:17:"14-1-300x200.jpeg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:17:"14-1-768x512.jpeg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:18:"14-1-1024x682.jpeg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(572, 122, '_wp_attached_file', '2018/03/13-1.jpeg'),
(573, 122, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:17:"2018/03/13-1.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"13-1-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:17:"13-1-300x200.jpeg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:17:"13-1-768x512.jpeg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:18:"13-1-1024x682.jpeg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(574, 123, '_wp_attached_file', '2018/03/12-1.jpeg'),
(575, 123, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:17:"2018/03/12-1.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"12-1-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:17:"12-1-300x200.jpeg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:17:"12-1-768x512.jpeg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:18:"12-1-1024x682.jpeg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(576, 124, '_wp_attached_file', '2018/03/11-1.jpeg'),
(577, 124, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:17:"2018/03/11-1.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"11-1-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:17:"11-1-300x200.jpeg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:17:"11-1-768x512.jpeg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:18:"11-1-1024x682.jpeg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(578, 125, '_wp_attached_file', '2018/03/10-1.jpeg'),
(579, 125, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:17:"2018/03/10-1.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:17:"10-1-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:17:"10-1-300x200.jpeg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:17:"10-1-768x512.jpeg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:18:"10-1-1024x682.jpeg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(580, 126, '_wp_attached_file', '2018/03/9-1.jpeg'),
(581, 126, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:16:"2018/03/9-1.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"9-1-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:16:"9-1-300x200.jpeg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:16:"9-1-768x512.jpeg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:17:"9-1-1024x682.jpeg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(582, 127, '_wp_attached_file', '2018/03/8-1.jpeg'),
(583, 127, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:16:"2018/03/8-1.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"8-1-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:16:"8-1-300x200.jpeg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:16:"8-1-768x512.jpeg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:17:"8-1-1024x682.jpeg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(584, 128, '_wp_attached_file', '2018/03/7-1.jpeg'),
(585, 128, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:16:"2018/03/7-1.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"7-1-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:16:"7-1-300x200.jpeg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:16:"7-1-768x512.jpeg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:17:"7-1-1024x682.jpeg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(586, 129, '_wp_attached_file', '2018/03/6-1.jpg'),
(587, 129, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:6000;s:6:"height";i:4000;s:4:"file";s:15:"2018/03/6-1.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:15:"6-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:15:"6-1-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:15:"6-1-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:16:"6-1-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:2:"13";s:6:"credit";s:14:"Naveen Bhasima";s:6:"camera";s:11:"NIKON D7200";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1516108471";s:9:"copyright";s:18:"Naveen Photography";s:12:"focal_length";s:2:"40";s:3:"iso";s:3:"125";s:13:"shutter_speed";s:5:"0.008";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(588, 130, '_wp_attached_file', '2018/03/5.jpg'),
(589, 130, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:6000;s:6:"height";i:4000;s:4:"file";s:13:"2018/03/5.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:13:"5-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:13:"5-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:13:"5-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:14:"5-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:2:"16";s:6:"credit";s:14:"Naveen Bhasima";s:6:"camera";s:11:"NIKON D7200";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1516108402";s:9:"copyright";s:18:"Naveen Photography";s:12:"focal_length";s:2:"18";s:3:"iso";s:3:"125";s:13:"shutter_speed";s:5:"0.008";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(590, 131, '_wp_attached_file', '2018/03/4.jpg'),
(591, 131, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:4000;s:6:"height";i:6000;s:4:"file";s:13:"2018/03/4.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:13:"4-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:13:"4-200x300.jpg";s:5:"width";i:200;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:14:"4-768x1152.jpg";s:5:"width";i:768;s:6:"height";i:1152;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:14:"4-683x1024.jpg";s:5:"width";i:683;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:2:"29";s:6:"credit";s:14:"Naveen Bhasima";s:6:"camera";s:11:"NIKON D7200";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1516108216";s:9:"copyright";s:18:"Naveen Photography";s:12:"focal_length";s:3:"140";s:3:"iso";s:3:"640";s:13:"shutter_speed";s:5:"0.008";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(592, 132, '_wp_attached_file', '2018/03/3.jpg'),
(593, 132, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:6000;s:6:"height";i:4000;s:4:"file";s:13:"2018/03/3.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:13:"3-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:13:"3-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:13:"3-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:14:"3-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:2:"22";s:6:"credit";s:14:"Naveen Bhasima";s:6:"camera";s:11:"NIKON D7200";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1516107897";s:9:"copyright";s:18:"Naveen Photography";s:12:"focal_length";s:2:"95";s:3:"iso";s:3:"320";s:13:"shutter_speed";s:5:"0.008";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(594, 133, '_wp_attached_file', '2018/03/2.jpeg'),
(595, 133, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:853;s:6:"height";i:1280;s:4:"file";s:14:"2018/03/2.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:14:"2-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:14:"2-200x300.jpeg";s:5:"width";i:200;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:15:"2-768x1152.jpeg";s:5:"width";i:768;s:6:"height";i:1152;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:15:"2-682x1024.jpeg";s:5:"width";i:682;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(596, 134, '_wp_attached_file', '2018/03/1.jpeg'),
(597, 134, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:14:"2018/03/1.jpeg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:14:"1-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:14:"1-300x200.jpeg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:14:"1-768x512.jpeg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:15:"1-1024x682.jpeg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(598, 55, '_wp_page_template', 'templates/gallery.php'),
(599, 55, 'gallery_files_0_images', '112'),
(600, 55, '_gallery_files_0_images', 'field_5aa0f112c248a'),
(601, 55, 'gallery_files_1_images', '113'),
(602, 55, '_gallery_files_1_images', 'field_5aa0f112c248a'),
(603, 55, 'gallery_files_2_images', '114'),
(604, 55, '_gallery_files_2_images', 'field_5aa0f112c248a'),
(605, 55, 'gallery_files_3_images', '115'),
(606, 55, '_gallery_files_3_images', 'field_5aa0f112c248a'),
(607, 55, 'gallery_files_4_images', '116'),
(608, 55, '_gallery_files_4_images', 'field_5aa0f112c248a'),
(609, 55, 'gallery_files_5_images', '117'),
(610, 55, '_gallery_files_5_images', 'field_5aa0f112c248a'),
(611, 55, 'gallery_files_6_images', '118'),
(612, 55, '_gallery_files_6_images', 'field_5aa0f112c248a'),
(613, 55, 'gallery_files_7_images', '119'),
(614, 55, '_gallery_files_7_images', 'field_5aa0f112c248a'),
(615, 55, 'gallery_files_8_images', '120'),
(616, 55, '_gallery_files_8_images', 'field_5aa0f112c248a'),
(617, 55, 'gallery_files_9_images', '121'),
(618, 55, '_gallery_files_9_images', 'field_5aa0f112c248a'),
(619, 55, 'gallery_files_10_images', '122'),
(620, 55, '_gallery_files_10_images', 'field_5aa0f112c248a'),
(621, 55, 'gallery_files_11_images', '123'),
(622, 55, '_gallery_files_11_images', 'field_5aa0f112c248a'),
(623, 55, 'gallery_files_12_images', '124'),
(624, 55, '_gallery_files_12_images', 'field_5aa0f112c248a'),
(625, 55, 'gallery_files_13_images', '125'),
(626, 55, '_gallery_files_13_images', 'field_5aa0f112c248a'),
(627, 55, 'gallery_files_14_images', '126'),
(628, 55, '_gallery_files_14_images', 'field_5aa0f112c248a'),
(629, 55, 'gallery_files_15_images', '127'),
(630, 55, '_gallery_files_15_images', 'field_5aa0f112c248a'),
(631, 55, 'gallery_files_16_images', '128'),
(632, 55, '_gallery_files_16_images', 'field_5aa0f112c248a'),
(633, 55, 'gallery_files_17_images', '129'),
(634, 55, '_gallery_files_17_images', 'field_5aa0f112c248a'),
(635, 55, 'gallery_files_18_images', '130'),
(636, 55, '_gallery_files_18_images', 'field_5aa0f112c248a'),
(637, 55, 'gallery_files_19_images', '131'),
(638, 55, '_gallery_files_19_images', 'field_5aa0f112c248a'),
(639, 55, 'gallery_files_20_images', '132'),
(640, 55, '_gallery_files_20_images', 'field_5aa0f112c248a'),
(641, 55, 'gallery_files_21_images', '133'),
(642, 55, '_gallery_files_21_images', 'field_5aa0f112c248a'),
(643, 55, 'gallery_files_22_images', '134'),
(644, 55, '_gallery_files_22_images', 'field_5aa0f112c248a'),
(645, 55, 'gallery_files', '23'),
(646, 55, '_gallery_files', 'field_5aa0f0ebc2489'),
(647, 135, 'gallery_files_0_images', '112'),
(648, 135, '_gallery_files_0_images', 'field_5aa0f112c248a'),
(649, 135, 'gallery_files_1_images', '113'),
(650, 135, '_gallery_files_1_images', 'field_5aa0f112c248a'),
(651, 135, 'gallery_files_2_images', '114'),
(652, 135, '_gallery_files_2_images', 'field_5aa0f112c248a'),
(653, 135, 'gallery_files_3_images', '115'),
(654, 135, '_gallery_files_3_images', 'field_5aa0f112c248a'),
(655, 135, 'gallery_files_4_images', '116'),
(656, 135, '_gallery_files_4_images', 'field_5aa0f112c248a'),
(657, 135, 'gallery_files_5_images', '117'),
(658, 135, '_gallery_files_5_images', 'field_5aa0f112c248a'),
(659, 135, 'gallery_files_6_images', '118'),
(660, 135, '_gallery_files_6_images', 'field_5aa0f112c248a'),
(661, 135, 'gallery_files_7_images', '119'),
(662, 135, '_gallery_files_7_images', 'field_5aa0f112c248a'),
(663, 135, 'gallery_files_8_images', '120'),
(664, 135, '_gallery_files_8_images', 'field_5aa0f112c248a'),
(665, 135, 'gallery_files_9_images', '121'),
(666, 135, '_gallery_files_9_images', 'field_5aa0f112c248a'),
(667, 135, 'gallery_files_10_images', '122'),
(668, 135, '_gallery_files_10_images', 'field_5aa0f112c248a'),
(669, 135, 'gallery_files_11_images', '123'),
(670, 135, '_gallery_files_11_images', 'field_5aa0f112c248a'),
(671, 135, 'gallery_files_12_images', '124'),
(672, 135, '_gallery_files_12_images', 'field_5aa0f112c248a'),
(673, 135, 'gallery_files_13_images', '125'),
(674, 135, '_gallery_files_13_images', 'field_5aa0f112c248a'),
(675, 135, 'gallery_files_14_images', '126'),
(676, 135, '_gallery_files_14_images', 'field_5aa0f112c248a'),
(677, 135, 'gallery_files_15_images', '127'),
(678, 135, '_gallery_files_15_images', 'field_5aa0f112c248a'),
(679, 135, 'gallery_files_16_images', '128'),
(680, 135, '_gallery_files_16_images', 'field_5aa0f112c248a'),
(681, 135, 'gallery_files_17_images', '129'),
(682, 135, '_gallery_files_17_images', 'field_5aa0f112c248a'),
(683, 135, 'gallery_files_18_images', '130'),
(684, 135, '_gallery_files_18_images', 'field_5aa0f112c248a'),
(685, 135, 'gallery_files_19_images', '131'),
(686, 135, '_gallery_files_19_images', 'field_5aa0f112c248a'),
(687, 135, 'gallery_files_20_images', '132'),
(688, 135, '_gallery_files_20_images', 'field_5aa0f112c248a'),
(689, 135, 'gallery_files_21_images', '133'),
(690, 135, '_gallery_files_21_images', 'field_5aa0f112c248a'),
(691, 135, 'gallery_files_22_images', '134'),
(692, 135, '_gallery_files_22_images', 'field_5aa0f112c248a'),
(693, 135, 'gallery_files', '23'),
(694, 135, '_gallery_files', 'field_5aa0f0ebc2489'),
(695, 59, '_wp_page_template', 'templates/involved.php'),
(696, 136, '_form', '<div class="col-md-6">\n  <label>Full Name :</label>\n [text* name id:name class:form-control]\n  </div>\n           \n  <div class="col-md-6">\n    <label>Email :</label>\n   [email* email id:email class:form-control]\n   </div>\n                \n<div class="col-md-6">\n <label>Phone :</label>\n [number* number-551 id:phone class:form-control]\n </div>\n                \n<div class="col-md-6">\n<label>Country/City :</label>\n[text* country id:country class:form-control]\n</div>\n                \n<div class="clearfix"></div>\n <div class="col-sm-12">\n <label>Message :</label>\n [textarea* comment id:comment class:form-control]\n  </div>\n                \n<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-btm-1">\n <i class="fa fa-paper-plane" aria-hidden="true"></i>\n </button>\n </div>'),
(697, 136, '_mail', 'a:9:{s:6:"active";b:1;s:7:"subject";s:32:"Himalaya Biking "[your-subject]"";s:6:"sender";s:41:"[your-name] <pracriti11neupane@gmail.com>";s:9:"recipient";s:27:"pracriti11neupane@gmail.com";s:4:"body";s:194:"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Himalaya Biking (http://localhost/office_projects/cycling)";s:18:"additional_headers";s:22:"Reply-To: [your-email]";s:11:"attachments";s:0:"";s:8:"use_html";b:0;s:13:"exclude_blank";b:0;}'),
(698, 136, '_mail_2', 'a:9:{s:6:"active";b:0;s:7:"subject";s:32:"Himalaya Biking "[your-subject]"";s:6:"sender";s:45:"Himalaya Biking <pracriti11neupane@gmail.com>";s:9:"recipient";s:12:"[your-email]";s:4:"body";s:136:"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Himalaya Biking (http://localhost/office_projects/cycling)";s:18:"additional_headers";s:37:"Reply-To: pracriti11neupane@gmail.com";s:11:"attachments";s:0:"";s:8:"use_html";b:0;s:13:"exclude_blank";b:0;}'),
(699, 136, '_messages', 'a:23:{s:12:"mail_sent_ok";s:45:"Thank you for your message. It has been sent.";s:12:"mail_sent_ng";s:71:"There was an error trying to send your message. Please try again later.";s:16:"validation_error";s:61:"One or more fields have an error. Please check and try again.";s:4:"spam";s:71:"There was an error trying to send your message. Please try again later.";s:12:"accept_terms";s:69:"You must accept the terms and conditions before sending your message.";s:16:"invalid_required";s:22:"The field is required.";s:16:"invalid_too_long";s:22:"The field is too long.";s:17:"invalid_too_short";s:23:"The field is too short.";s:12:"invalid_date";s:29:"The date format is incorrect.";s:14:"date_too_early";s:44:"The date is before the earliest one allowed.";s:13:"date_too_late";s:41:"The date is after the latest one allowed.";s:13:"upload_failed";s:46:"There was an unknown error uploading the file.";s:24:"upload_file_type_invalid";s:49:"You are not allowed to upload files of this type.";s:21:"upload_file_too_large";s:20:"The file is too big.";s:23:"upload_failed_php_error";s:38:"There was an error uploading the file.";s:14:"invalid_number";s:29:"The number format is invalid.";s:16:"number_too_small";s:47:"The number is smaller than the minimum allowed.";s:16:"number_too_large";s:46:"The number is larger than the maximum allowed.";s:23:"quiz_answer_not_correct";s:36:"The answer to the quiz is incorrect.";s:17:"captcha_not_match";s:31:"Your entered code is incorrect.";s:13:"invalid_email";s:38:"The e-mail address entered is invalid.";s:11:"invalid_url";s:19:"The URL is invalid.";s:11:"invalid_tel";s:32:"The telephone number is invalid.";}'),
(700, 136, '_additional_settings', ''),
(701, 136, '_locale', 'en_US'),
(704, 136, '_config_errors', 'a:2:{s:9:"form.body";a:1:{i:0;a:2:{s:4:"code";i:107;s:4:"args";a:3:{s:7:"message";s:55:"Unavailable names (%names%) are used for form controls.";s:6:"params";a:1:{s:5:"names";s:6:""name"";}s:4:"link";s:63:"https://contactform7.com/configuration-errors/unavailable-names";}}}s:23:"mail.additional_headers";a:1:{i:0;a:2:{s:4:"code";i:102;s:4:"args";a:3:{s:7:"message";s:51:"Invalid mailbox syntax is used in the %name% field.";s:6:"params";a:1:{s:4:"name";s:8:"Reply-To";}s:4:"link";s:68:"https://contactform7.com/configuration-errors/invalid-mailbox-syntax";}}}}');

-- --------------------------------------------------------

--
-- Table structure for table `web_posts`
--

CREATE TABLE `web_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `web_posts`
--

INSERT INTO `web_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2018-03-07 08:33:38', '2018-03-07 08:33:38', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2018-03-07 08:33:38', '2018-03-07 08:33:38', '', 0, 'http://localhost/office_projects/cycling/?p=1', 0, 'post', '', 1),
(2, 1, '2018-03-07 08:33:38', '2018-03-07 08:33:38', 'This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href="http://localhost/office_projects/cycling/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'trash', 'closed', 'open', '', 'sample-page__trashed', '', '', '2018-03-07 10:01:55', '2018-03-07 10:01:55', '', 0, 'http://localhost/office_projects/cycling/?page_id=2', 0, 'page', '', 0),
(5, 1, '2018-03-07 09:47:38', '2018-03-07 09:47:38', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:4:"page";s:8:"operator";s:2:"==";s:5:"value";s:1:"9";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";}', 'Home', 'home', 'publish', 'closed', 'closed', '', 'group_5a9fb52097394', '', '', '2018-03-07 12:27:33', '2018-03-07 12:27:33', '', 0, 'http://localhost/office_projects/cycling/?post_type=acf-field-group&#038;p=5', 0, 'acf-field-group', '', 0),
(6, 1, '2018-03-07 09:50:32', '2018-03-07 09:50:32', 'a:10:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"collapsed";s:0:"";s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"layout";s:5:"table";s:12:"button_label";s:7:"Add Row";}', 'Slider', 'slider', 'publish', 'closed', 'closed', '', 'field_5a9fb5405d1ef', '', '', '2018-03-07 09:50:32', '2018-03-07 09:50:32', '', 5, 'http://localhost/office_projects/cycling/?post_type=acf-field&p=6', 0, 'acf-field', '', 0),
(7, 1, '2018-03-07 09:50:32', '2018-03-07 09:50:32', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Slider Image', 'slider_image', 'publish', 'closed', 'closed', '', 'field_5a9fb5845d1f0', '', '', '2018-03-07 09:50:32', '2018-03-07 09:50:32', '', 6, 'http://localhost/office_projects/cycling/?post_type=acf-field&p=7', 0, 'acf-field', '', 0),
(8, 1, '2018-03-07 09:50:32', '2018-03-07 09:50:32', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Slider Content', 'slider_content', 'publish', 'closed', 'closed', '', 'field_5a9fb5ab5d1f1', '', '', '2018-03-07 09:50:32', '2018-03-07 09:50:32', '', 6, 'http://localhost/office_projects/cycling/?post_type=acf-field&p=8', 1, 'acf-field', '', 0),
(9, 1, '2018-03-07 09:52:32', '2018-03-07 09:52:32', '<h2>Himalayan Biking Adventures</h2>\r\nSed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2018-03-08 05:22:21', '2018-03-08 05:22:21', '', 0, 'http://localhost/office_projects/cycling/?page_id=9', 0, 'page', '', 0),
(10, 1, '2018-03-07 09:52:32', '2018-03-07 09:52:32', '', 'Home', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2018-03-07 09:52:32', '2018-03-07 09:52:32', '', 9, 'http://localhost/office_projects/cycling/2018/03/07/9-revision-v1/', 0, 'revision', '', 0),
(11, 1, '2018-03-07 10:00:28', '2018-03-07 10:00:28', ' ', '', '', 'publish', 'closed', 'closed', '', '11', '', '', '2018-03-07 12:23:21', '2018-03-07 12:23:21', '', 0, 'http://localhost/office_projects/cycling/?p=11', 1, 'nav_menu_item', '', 0),
(12, 1, '2018-03-07 10:01:55', '2018-03-07 10:01:55', 'This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href="http://localhost/office_projects/cycling/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-03-07 10:01:55', '2018-03-07 10:01:55', '', 2, 'http://localhost/office_projects/cycling/2018/03/07/2-revision-v1/', 0, 'revision', '', 0),
(13, 1, '2018-03-07 10:05:01', '2018-03-07 10:05:01', '', 'banner5', '', 'inherit', 'open', 'closed', '', 'banner5', '', '', '2018-03-07 10:05:15', '2018-03-07 10:05:15', '', 9, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/banner5.jpg', 0, 'attachment', 'image/jpeg', 0),
(14, 1, '2018-03-07 10:05:03', '2018-03-07 10:05:03', '', 'banner4', '', 'inherit', 'open', 'closed', '', 'banner4', '', '', '2018-03-07 10:05:03', '2018-03-07 10:05:03', '', 9, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/banner4.jpg', 0, 'attachment', 'image/jpeg', 0),
(15, 1, '2018-03-07 10:05:05', '2018-03-07 10:05:05', '', 'banner3', '', 'inherit', 'open', 'closed', '', 'banner3', '', '', '2018-03-07 10:05:05', '2018-03-07 10:05:05', '', 9, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/banner3.jpg', 0, 'attachment', 'image/jpeg', 0),
(16, 1, '2018-03-07 10:05:07', '2018-03-07 10:05:07', '', 'banner2', '', 'inherit', 'open', 'closed', '', 'banner2', '', '', '2018-03-07 10:05:07', '2018-03-07 10:05:07', '', 9, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/banner2.jpg', 0, 'attachment', 'image/jpeg', 0),
(17, 1, '2018-03-07 10:05:09', '2018-03-07 10:05:09', '', 'banner1', '', 'inherit', 'open', 'closed', '', 'banner1', '', '', '2018-03-07 10:05:09', '2018-03-07 10:05:09', '', 9, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/banner1.jpg', 0, 'attachment', 'image/jpeg', 0),
(18, 1, '2018-03-07 10:07:25', '2018-03-07 10:07:25', '', 'Home', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2018-03-07 10:07:25', '2018-03-07 10:07:25', '', 9, 'http://localhost/office_projects/cycling/2018/03/07/9-revision-v1/', 0, 'revision', '', 0),
(19, 1, '2018-03-07 10:21:41', '2018-03-07 10:21:41', '<h2>Himalayan Biking Adventures</h2>\nSed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.', 'Home', '', 'inherit', 'closed', 'closed', '', '9-autosave-v1', '', '', '2018-03-07 10:21:41', '2018-03-07 10:21:41', '', 9, 'http://localhost/office_projects/cycling/2018/03/07/9-autosave-v1/', 0, 'revision', '', 0),
(20, 1, '2018-03-07 10:21:48', '2018-03-07 10:21:48', '<h2>Himalayan Biking Adventures</h2>\r\nSed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.', 'Home', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2018-03-07 10:21:48', '2018-03-07 10:21:48', '', 9, 'http://localhost/office_projects/cycling/2018/03/07/9-revision-v1/', 0, 'revision', '', 0),
(21, 1, '2018-03-07 10:24:49', '2018-03-07 10:24:49', '', 'cyclist', '', 'inherit', 'open', 'closed', '', 'cyclist', '', '', '2018-03-07 10:24:49', '2018-03-07 10:24:49', '', 9, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/cyclist.png', 0, 'attachment', 'image/png', 0),
(22, 1, '2018-03-07 10:34:55', '2018-03-07 10:34:55', 'a:10:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"collapsed";s:0:"";s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"layout";s:5:"table";s:12:"button_label";s:7:"Add Row";}', 'Below Biking Adventure', 'below_biking_adventure', 'publish', 'closed', 'closed', '', 'field_5a9fbfdc42ea5', '', '', '2018-03-07 10:34:55', '2018-03-07 10:34:55', '', 5, 'http://localhost/office_projects/cycling/?post_type=acf-field&p=22', 1, 'acf-field', '', 0),
(23, 1, '2018-03-07 10:38:12', '2018-03-07 10:38:12', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'images', 'images', 'publish', 'closed', 'closed', '', 'field_5a9fc0fc49888', '', '', '2018-03-07 10:38:12', '2018-03-07 10:38:12', '', 22, 'http://localhost/office_projects/cycling/?post_type=acf-field&p=23', 0, 'acf-field', '', 0),
(24, 1, '2018-03-07 10:40:50', '2018-03-07 10:40:50', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Image Text', 'image_text', 'publish', 'closed', 'closed', '', 'field_5a9fc1a104d48', '', '', '2018-03-07 10:40:50', '2018-03-07 10:40:50', '', 22, 'http://localhost/office_projects/cycling/?post_type=acf-field&p=24', 1, 'acf-field', '', 0),
(25, 1, '2018-03-07 10:42:20', '2018-03-07 10:42:20', '', 'cycle1', '', 'inherit', 'open', 'closed', '', 'cycle1', '', '', '2018-03-08 05:22:11', '2018-03-08 05:22:11', '', 9, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/cycle1.png', 0, 'attachment', 'image/png', 0),
(26, 1, '2018-03-07 10:42:59', '2018-03-07 10:42:59', '<h2>Himalayan Biking Adventures</h2>\r\nSed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.', 'Home', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2018-03-07 10:42:59', '2018-03-07 10:42:59', '', 9, 'http://localhost/office_projects/cycling/2018/03/07/9-revision-v1/', 0, 'revision', '', 0),
(27, 1, '2018-03-07 11:10:45', '2018-03-07 11:10:45', '<h2>Himalayan Biking Adventures</h2>\r\nSed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.', 'Home', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2018-03-07 11:10:45', '2018-03-07 11:10:45', '', 9, 'http://localhost/office_projects/cycling/2018/03/07/9-revision-v1/', 0, 'revision', '', 0),
(28, 1, '2018-03-07 11:14:34', '2018-03-07 11:14:34', '<h2>Himalayan Biking Adventures</h2>\r\nSed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.', 'Home', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2018-03-07 11:14:34', '2018-03-07 11:14:34', '', 9, 'http://localhost/office_projects/cycling/2018/03/07/9-revision-v1/', 0, 'revision', '', 0),
(30, 1, '2018-03-07 11:41:37', '2018-03-07 11:41:37', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:12:"options_page";s:8:"operator";s:2:"==";s:5:"value";s:22:"theme-general-settings";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";}', 'General Fields', 'general-fields', 'publish', 'closed', 'closed', '', 'group_5a9fcfcd37e89', '', '', '2018-03-07 11:50:58', '2018-03-07 11:50:58', '', 0, 'http://localhost/office_projects/cycling/?post_type=acf-field-group&#038;p=30', 0, 'acf-field-group', '', 0),
(31, 1, '2018-03-07 11:42:39', '2018-03-07 11:42:39', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:3:"url";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Header Logo', 'header_logo', 'publish', 'closed', 'closed', '', 'field_5a9fd01c0e411', '', '', '2018-03-07 11:50:33', '2018-03-07 11:50:33', '', 30, 'http://localhost/office_projects/cycling/?post_type=acf-field&#038;p=31', 0, 'acf-field', '', 0),
(32, 1, '2018-03-07 11:43:36', '2018-03-07 11:43:36', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:3:"url";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Footer Logo', 'footer_logo', 'publish', 'closed', 'closed', '', 'field_5a9fd037ed62a', '', '', '2018-03-07 11:50:58', '2018-03-07 11:50:58', '', 30, 'http://localhost/office_projects/cycling/?post_type=acf-field&#038;p=32', 1, 'acf-field', '', 0),
(33, 1, '2018-03-07 11:44:56', '2018-03-07 11:44:56', '', 'logo2', '', 'inherit', 'open', 'closed', '', 'logo2', '', '', '2018-03-07 11:44:59', '2018-03-07 11:44:59', '', 0, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/logo2.png', 0, 'attachment', 'image/png', 0),
(34, 1, '2018-03-07 11:45:09', '2018-03-07 11:45:09', '', 'footer-logo', '', 'inherit', 'open', 'closed', '', 'footer-logo', '', '', '2018-03-07 11:45:10', '2018-03-07 11:45:10', '', 0, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/footer-logo.png', 0, 'attachment', 'image/png', 0),
(35, 1, '2018-03-07 11:53:16', '2018-03-07 11:53:16', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:12:"options_page";s:8:"operator";s:2:"==";s:5:"value";s:29:"acf-options-social-media-link";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";}', 'Social Links & Address', 'social-links-address', 'publish', 'closed', 'closed', '', 'group_5a9fd2a18a1bd', '', '', '2018-03-07 11:56:33', '2018-03-07 11:56:33', '', 0, 'http://localhost/office_projects/cycling/?post_type=acf-field-group&#038;p=35', 0, 'acf-field-group', '', 0),
(36, 1, '2018-03-07 11:53:51', '2018-03-07 11:53:51', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Address', 'address', 'publish', 'closed', 'closed', '', 'field_5a9fd2c3d0d43', '', '', '2018-03-07 11:53:51', '2018-03-07 11:53:51', '', 35, 'http://localhost/office_projects/cycling/?post_type=acf-field&p=36', 0, 'acf-field', '', 0),
(37, 1, '2018-03-07 11:54:25', '2018-03-07 11:54:25', 'a:9:{s:4:"type";s:5:"email";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";}', 'Email', 'email', 'publish', 'closed', 'closed', '', 'field_5a9fd2e70c00a', '', '', '2018-03-07 11:54:25', '2018-03-07 11:54:25', '', 35, 'http://localhost/office_projects/cycling/?post_type=acf-field&p=37', 1, 'acf-field', '', 0),
(38, 1, '2018-03-07 11:54:39', '2018-03-07 11:54:39', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Phone Number', 'phone_number', 'publish', 'closed', 'closed', '', 'field_5a9fd2f6d9fd0', '', '', '2018-03-07 11:54:39', '2018-03-07 11:54:39', '', 35, 'http://localhost/office_projects/cycling/?post_type=acf-field&p=38', 2, 'acf-field', '', 0),
(39, 1, '2018-03-07 11:55:19', '2018-03-07 11:55:19', 'a:7:{s:4:"type";s:3:"url";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";}', 'Facebook Link', 'facebook_link', 'publish', 'closed', 'closed', '', 'field_5a9fd3169d613', '', '', '2018-03-07 11:55:19', '2018-03-07 11:55:19', '', 35, 'http://localhost/office_projects/cycling/?post_type=acf-field&p=39', 3, 'acf-field', '', 0),
(40, 1, '2018-03-07 11:55:40', '2018-03-07 11:55:40', 'a:7:{s:4:"type";s:3:"url";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";}', 'Twitter Link', 'twitter_link', 'publish', 'closed', 'closed', '', 'field_5a9fd32d6577a', '', '', '2018-03-07 11:55:40', '2018-03-07 11:55:40', '', 35, 'http://localhost/office_projects/cycling/?post_type=acf-field&p=40', 4, 'acf-field', '', 0),
(41, 1, '2018-03-07 11:56:06', '2018-03-07 11:56:06', 'a:7:{s:4:"type";s:3:"url";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";}', 'Instagram Link', 'instagram_link', 'publish', 'closed', 'closed', '', 'field_5a9fd346c535a', '', '', '2018-03-07 11:56:06', '2018-03-07 11:56:06', '', 35, 'http://localhost/office_projects/cycling/?post_type=acf-field&p=41', 5, 'acf-field', '', 0),
(42, 1, '2018-03-07 11:56:33', '2018-03-07 11:56:33', 'a:7:{s:4:"type";s:3:"url";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";}', 'Google Plus Link', 'google_plus_link', 'publish', 'closed', 'closed', '', 'field_5a9fd35c80e04', '', '', '2018-03-07 11:56:33', '2018-03-07 11:56:33', '', 35, 'http://localhost/office_projects/cycling/?post_type=acf-field&p=42', 6, 'acf-field', '', 0),
(43, 1, '2018-03-07 12:03:39', '2018-03-07 12:03:39', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Video Link', 'video_link', 'publish', 'closed', 'closed', '', 'field_5a9fd50560035', '', '', '2018-03-07 12:03:39', '2018-03-07 12:03:39', '', 5, 'http://localhost/office_projects/cycling/?post_type=acf-field&p=43', 2, 'acf-field', '', 0),
(44, 1, '2018-03-07 12:04:39', '2018-03-07 12:04:39', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Video Code', 'video_code', 'publish', 'closed', 'closed', '', 'field_5a9fd5222acca', '', '', '2018-03-07 12:04:39', '2018-03-07 12:04:39', '', 5, 'http://localhost/office_projects/cycling/?post_type=acf-field&p=44', 3, 'acf-field', '', 0),
(45, 1, '2018-03-07 12:05:05', '2018-03-07 12:05:05', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Video Image', 'video_image', 'publish', 'closed', 'closed', '', 'field_5a9fd55eb1e7d', '', '', '2018-03-07 12:05:05', '2018-03-07 12:05:05', '', 5, 'http://localhost/office_projects/cycling/?post_type=acf-field&p=45', 4, 'acf-field', '', 0),
(46, 1, '2018-03-07 12:07:36', '2018-03-07 12:07:36', '', 'youtube', '', 'inherit', 'open', 'closed', '', 'youtube', '', '', '2018-03-07 12:07:37', '2018-03-07 12:07:37', '', 9, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/youtube.jpg', 0, 'attachment', 'image/jpeg', 0),
(47, 1, '2018-03-07 12:07:45', '2018-03-07 12:07:45', '<h2>Himalayan Biking Adventures</h2>\r\nSed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.', 'Home', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2018-03-07 12:07:45', '2018-03-07 12:07:45', '', 9, 'http://localhost/office_projects/cycling/2018/03/07/9-revision-v1/', 0, 'revision', '', 0),
(48, 1, '2018-03-07 12:13:36', '2018-03-07 12:13:36', 'a:9:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;}', 'Journey Stories', 'journey_stories', 'publish', 'closed', 'closed', '', 'field_5a9fd736baff4', '', '', '2018-03-07 12:13:36', '2018-03-07 12:13:36', '', 5, 'http://localhost/office_projects/cycling/?post_type=acf-field&p=48', 5, 'acf-field', '', 0),
(49, 1, '2018-03-07 12:15:10', '2018-03-07 12:15:10', '<h2>Himalayan Biking Adventures</h2>\r\nSed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.', 'Home', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2018-03-07 12:15:10', '2018-03-07 12:15:10', '', 9, 'http://localhost/office_projects/cycling/2018/03/07/9-revision-v1/', 0, 'revision', '', 0),
(50, 1, '2018-03-07 12:17:11', '2018-03-07 12:17:11', '&nbsp;\r\n<h2>Himalayan Biking Adventure In Nagarkot</h2>\r\n<div class="mt-content intro-content">\r\n\r\nNagarkot is a rocking Mountain Trail Biking destination. We’ll take this to greater heights this time with our Himalayan biking adventure challenge with inspiring views of the himalayas. Blaze through the trails going uphill, downhill with villages, terraced farmlands and forests on the way.\r\n\r\nScenic uphill ride to the 360 view tower with an elevation gain of 200m from where you can enjoy the unparalleled Himalayan vistas, 5 out of 8 highest peaks of Nepal can been seen from here in a crystal clear day (Everest not to be forgotten) along with lots of 7000’s meter and 6000’s meter high peak. After the visit to the view tower, we will pass through Serene trail inside pine forest with occasional bird watching opportunity as well. (Please follow the map as it can be confusing for you). Then, the trail passes through different villages commanding unobstructed views of rolling hills, verdant valleys, lush forests, snowcapped himalayas.\r\n\r\nScenic uphill ride to the 360 view tower with an elevation gain of 200m from where you can enjoy the unparalleled Himalayan vistas, 5 out of 8 highest peaks of Nepal can been seen from here in a crystal clear day (Everest not to be forgotten) along with lots of 7000’s meter and 6000’s meter high peak. After the visit to the view tower, we will pass through Serene trail inside pine forest with occasional bird watching opportunity as well. (Please follow the map as it can be confusing for you). Then, the trail passes through different villages commanding unobstructed views of rolling hills, verdant valleys, lush forests, snowcapped himalayas.\r\n\r\n</div>', 'About Us', '', 'publish', 'closed', 'closed', '', 'about-us', '', '', '2018-03-08 06:11:49', '2018-03-08 06:11:49', '', 0, 'http://localhost/office_projects/cycling/?page_id=50', 0, 'page', '', 0),
(51, 1, '2018-03-07 12:17:11', '2018-03-07 12:17:11', '', 'About Us', '', 'inherit', 'closed', 'closed', '', '50-revision-v1', '', '', '2018-03-07 12:17:11', '2018-03-07 12:17:11', '', 50, 'http://localhost/office_projects/cycling/2018/03/07/50-revision-v1/', 0, 'revision', '', 0),
(52, 1, '2018-03-07 12:17:41', '2018-03-07 12:17:41', 'Nagarkot is a rocking Mountain Trail Biking destination. We’ll take this to greater heights this time with our Himalayan biking adventure challenge with inspiring views of the himalayas. Blaze through the trails going uphill, downhill with villages, terraced farmlands and forests on the way.\r\n\r\nScenic uphill ride to the 360 view tower with an elevation gain of 200m from where you can enjoy the unparalleled Himalayan vistas, 5 out of 8 highest peaks of Nepal can been seen from here in a crystal clear day (Everest not to be forgotten) along with lots of 7000’s meter and 6000’s meter high peak. After the visit to the view tower, we will pass through Serene trail inside pine forest with occasional bird watching opportunity as well. (Please follow the map as it can be confusing for you). Then, the trail passes through different villages commanding unobstructed views of rolling hills, verdant valleys, lush forests, snowcapped himalayas.', 'Biking Trail', '', 'publish', 'closed', 'closed', '', 'biking-trail', '', '', '2018-03-08 08:05:35', '2018-03-08 08:05:35', '', 0, 'http://localhost/office_projects/cycling/?page_id=52', 0, 'page', '', 0),
(53, 1, '2018-03-07 12:17:41', '2018-03-07 12:17:41', '', 'Biking Trail', '', 'inherit', 'closed', 'closed', '', '52-revision-v1', '', '', '2018-03-07 12:17:41', '2018-03-07 12:17:41', '', 52, 'http://localhost/office_projects/cycling/2018/03/07/52-revision-v1/', 0, 'revision', '', 0),
(55, 1, '2018-03-07 12:18:02', '2018-03-07 12:18:02', '', 'Gallery', '', 'publish', 'closed', 'closed', '', 'gallery', '', '', '2018-03-08 08:20:07', '2018-03-08 08:20:07', '', 0, 'http://localhost/office_projects/cycling/?page_id=55', 0, 'page', '', 0),
(56, 1, '2018-03-07 12:18:02', '2018-03-07 12:18:02', '', 'Gallery', '', 'inherit', 'closed', 'closed', '', '55-revision-v1', '', '', '2018-03-07 12:18:02', '2018-03-07 12:18:02', '', 55, 'http://localhost/office_projects/cycling/2018/03/07/55-revision-v1/', 0, 'revision', '', 0),
(57, 1, '2018-03-07 12:18:22', '2018-03-07 12:18:22', '', 'Events', '', 'publish', 'closed', 'closed', '', 'events', '', '', '2018-03-07 12:18:22', '2018-03-07 12:18:22', '', 0, 'http://localhost/office_projects/cycling/?page_id=57', 0, 'page', '', 0),
(58, 1, '2018-03-07 12:18:22', '2018-03-07 12:18:22', '', 'Events', '', 'inherit', 'closed', 'closed', '', '57-revision-v1', '', '', '2018-03-07 12:18:22', '2018-03-07 12:18:22', '', 57, 'http://localhost/office_projects/cycling/2018/03/07/57-revision-v1/', 0, 'revision', '', 0),
(59, 1, '2018-03-07 12:18:45', '2018-03-07 12:18:45', '', 'Get Involved', '', 'publish', 'closed', 'closed', '', 'get-involved', '', '', '2018-03-08 08:33:53', '2018-03-08 08:33:53', '', 0, 'http://localhost/office_projects/cycling/?page_id=59', 0, 'page', '', 0),
(60, 1, '2018-03-07 12:18:45', '2018-03-07 12:18:45', '', 'Get Involved', '', 'inherit', 'closed', 'closed', '', '59-revision-v1', '', '', '2018-03-07 12:18:45', '2018-03-07 12:18:45', '', 59, 'http://localhost/office_projects/cycling/2018/03/07/59-revision-v1/', 0, 'revision', '', 0),
(61, 1, '2018-03-07 12:19:39', '2018-03-07 12:19:39', ' ', '', '', 'publish', 'closed', 'closed', '', '61', '', '', '2018-03-07 12:23:21', '2018-03-07 12:23:21', '', 0, 'http://localhost/office_projects/cycling/?p=61', 2, 'nav_menu_item', '', 0),
(62, 1, '2018-03-07 12:19:39', '2018-03-07 12:19:39', ' ', '', '', 'publish', 'closed', 'closed', '', '62', '', '', '2018-03-07 12:23:21', '2018-03-07 12:23:21', '', 0, 'http://localhost/office_projects/cycling/?p=62', 3, 'nav_menu_item', '', 0),
(63, 1, '2018-03-07 12:19:40', '2018-03-07 12:19:40', ' ', '', '', 'publish', 'closed', 'closed', '', '63', '', '', '2018-03-07 12:23:21', '2018-03-07 12:23:21', '', 0, 'http://localhost/office_projects/cycling/?p=63', 5, 'nav_menu_item', '', 0),
(64, 1, '2018-03-07 12:19:40', '2018-03-07 12:19:40', ' ', '', '', 'publish', 'closed', 'closed', '', '64', '', '', '2018-03-07 12:23:21', '2018-03-07 12:23:21', '', 0, 'http://localhost/office_projects/cycling/?p=64', 4, 'nav_menu_item', '', 0),
(65, 1, '2018-03-07 12:19:40', '2018-03-07 12:19:40', ' ', '', '', 'publish', 'closed', 'closed', '', '65', '', '', '2018-03-07 12:23:21', '2018-03-07 12:23:21', '', 0, 'http://localhost/office_projects/cycling/?p=65', 6, 'nav_menu_item', '', 0),
(66, 1, '2018-03-07 12:26:33', '2018-03-07 12:26:33', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Sidebar Image below biking adventure', 'sidebar_image_below_biking_adventure', 'publish', 'closed', 'closed', '', 'field_5a9fda61bc8bc', '', '', '2018-03-07 12:27:33', '2018-03-07 12:27:33', '', 5, 'http://localhost/office_projects/cycling/?post_type=acf-field&#038;p=66', 6, 'acf-field', '', 0),
(67, 1, '2018-03-07 12:28:21', '2018-03-07 12:28:21', '', 'slide1', '', 'inherit', 'open', 'closed', '', 'slide1', '', '', '2018-03-07 12:28:24', '2018-03-07 12:28:24', '', 9, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/slide1.jpg', 0, 'attachment', 'image/jpeg', 0),
(68, 1, '2018-03-07 12:28:34', '2018-03-07 12:28:34', '<h2>Himalayan Biking Adventures</h2>\r\nSed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.', 'Home', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2018-03-07 12:28:34', '2018-03-07 12:28:34', '', 9, 'http://localhost/office_projects/cycling/2018/03/07/9-revision-v1/', 0, 'revision', '', 0),
(69, 1, '2018-03-08 05:22:21', '2018-03-08 05:22:21', '<h2>Himalayan Biking Adventures</h2>\r\nSed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.', 'Home', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2018-03-08 05:22:21', '2018-03-08 05:22:21', '', 9, 'http://localhost/office_projects/cycling/2018/03/08/9-revision-v1/', 0, 'revision', '', 0),
(70, 1, '2018-03-08 06:04:38', '2018-03-08 06:04:38', '', 'about-bg', '', 'inherit', 'open', 'closed', '', 'about-bg', '', '', '2018-03-08 06:04:38', '2018-03-08 06:04:38', '', 50, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/about-bg.jpg', 0, 'attachment', 'image/jpeg', 0),
(71, 1, '2018-03-08 06:06:23', '2018-03-08 06:06:23', '&nbsp;\n<h2>Himalayan biking adventure in Nagarkot</h2>\n<div class="mt-content intro-content">\n\nNagarkot is a rocking Mountain Trail Biking destination. We’ll take this to greater heights this time with our Himalayan biking adventure challenge with inspiring views of the himalayas. Blaze through the trails going uphill, downhill with villages, terraced farmlands and forests on the way.\n\nScenic uphill ride to the 360 view tower with an elevation gain of 200m from where you can enjoy the unparalleled Himalayan vistas, 5 out of 8 highest peaks of Nepal can been seen from here in a crystal clear day (Everest not to be forgotten) along with lots of 7000’s meter and 6000’s meter high peak. After the visit to the view tower, we will pass through Serene trail inside pine forest with occasional bird watching opportunity as well. (Please follow the map as it can be confusing for you). Then, the trail passes through different villages commanding unobstructed views of rolling hills, verdant valleys, lush forests, snowcapped himalayas.\n\nScenic uphill ride to the 360 view tower with an elevation gain of 200m from where you can enjoy the unparalleled Himalayan vistas, 5 out of 8 highest peaks of Nepal can been seen from here in a crystal clear day (Everest not to be forgotten) along with lots of 7000’s meter and 6000’s meter high peak. After the visit to the view tower, we will pass through Serene trail inside pine forest with occasional bird watching opportunity as well. (Please follow the map as it can be confusing for you). Then, the trail passes through different villages commanding unobstructed views of rolling hills, verdant valleys, lush forests, snowcapped himalayas.\n\n</div>', 'About Us', '', 'inherit', 'closed', 'closed', '', '50-autosave-v1', '', '', '2018-03-08 06:06:23', '2018-03-08 06:06:23', '', 50, 'http://localhost/office_projects/cycling/2018/03/08/50-autosave-v1/', 0, 'revision', '', 0),
(72, 1, '2018-03-08 06:06:41', '2018-03-08 06:06:41', '&nbsp;\r\n<h2>Himalayan Biking Adventure In Nagarkot</h2>\r\n<div class="mt-content intro-content">\r\n\r\nNagarkot is a rocking Mountain Trail Biking destination. We’ll take this to greater heights this time with our Himalayan biking adventure challenge with inspiring views of the himalayas. Blaze through the trails going uphill, downhill with villages, terraced farmlands and forests on the way.\r\n\r\nScenic uphill ride to the 360 view tower with an elevation gain of 200m from where you can enjoy the unparalleled Himalayan vistas, 5 out of 8 highest peaks of Nepal can been seen from here in a crystal clear day (Everest not to be forgotten) along with lots of 7000’s meter and 6000’s meter high peak. After the visit to the view tower, we will pass through Serene trail inside pine forest with occasional bird watching opportunity as well. (Please follow the map as it can be confusing for you). Then, the trail passes through different villages commanding unobstructed views of rolling hills, verdant valleys, lush forests, snowcapped himalayas.\r\n\r\nScenic uphill ride to the 360 view tower with an elevation gain of 200m from where you can enjoy the unparalleled Himalayan vistas, 5 out of 8 highest peaks of Nepal can been seen from here in a crystal clear day (Everest not to be forgotten) along with lots of 7000’s meter and 6000’s meter high peak. After the visit to the view tower, we will pass through Serene trail inside pine forest with occasional bird watching opportunity as well. (Please follow the map as it can be confusing for you). Then, the trail passes through different villages commanding unobstructed views of rolling hills, verdant valleys, lush forests, snowcapped himalayas.\r\n\r\n</div>', 'About Us', '', 'inherit', 'closed', 'closed', '', '50-revision-v1', '', '', '2018-03-08 06:06:41', '2018-03-08 06:06:41', '', 50, 'http://localhost/office_projects/cycling/2018/03/08/50-revision-v1/', 0, 'revision', '', 0),
(73, 1, '2018-03-08 06:18:14', '2018-03-08 06:18:14', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:12:"options_page";s:8:"operator";s:2:"==";s:5:"value";s:30:"acf-options-know-about-us-page";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";}', 'About Himalayan Biking', 'about-himalayan-biking', 'trash', 'closed', 'closed', '', 'group_5aa0d592c6848__trashed', '', '', '2018-03-08 06:28:31', '2018-03-08 06:28:31', '', 0, 'http://localhost/office_projects/cycling/?post_type=acf-field-group&#038;p=73', 0, 'acf-field-group', '', 0),
(74, 1, '2018-03-08 06:18:40', '2018-03-08 06:18:40', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:3:"url";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'About Sidebar Image', 'about_sidebar_image', 'publish', 'closed', 'closed', '', 'field_5aa0d5ab7acfd', '', '', '2018-03-08 06:27:50', '2018-03-08 06:27:50', '', 30, 'http://localhost/office_projects/cycling/?post_type=acf-field&#038;p=74', 0, 'acf-field', '', 0),
(75, 1, '2018-03-08 06:20:10', '2018-03-08 06:20:10', '', 'about-side', '', 'inherit', 'open', 'closed', '', 'about-side', '', '', '2018-03-08 06:20:26', '2018-03-08 06:20:26', '', 0, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/about-side.jpg', 0, 'attachment', 'image/jpeg', 0),
(76, 1, '2018-03-08 06:29:10', '2018-03-08 06:29:10', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:4:"page";s:8:"operator";s:2:"==";s:5:"value";s:2:"52";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";}', 'Biking Trial', 'biking-trial', 'publish', 'closed', 'closed', '', 'group_5aa0d81e3f1a7', '', '', '2018-03-08 08:04:14', '2018-03-08 08:04:14', '', 0, 'http://localhost/office_projects/cycling/?post_type=acf-field-group&#038;p=76', 0, 'acf-field-group', '', 0),
(77, 1, '2018-03-08 06:41:06', '2018-03-08 06:41:06', 'Nagarkot is a rocking Mountain Trail Biking destination. We’ll take this to greater heights this time with our Himalayan biking adventure challenge with inspiring views of the himalayas. Blaze through the trails going uphill, downhill with villages, terraced farmlands and forests on the way.\n\nScenic uphill ride to the 360 view tower with an elevation gain of 200m from where you can enjoy the unparalleled Himalayan vistas, 5 out of 8 highest peaks of Nepal can been seen from here in a crystal clear day (Everest not to be forgotten) along with lots of 7000’s meter and 6000’s meter high peak. After the visit to the view tower, we will pass through Serene trail inside pine forest with occasional bird watching opportunity as well. (Please follow the map as it can be confusing for you). Then, the trail passes through different villages commanding unobstructed views of rolling hills, verdant valleys, lush forests, snowcapped himalayas.', 'Biking Trail', '', 'inherit', 'closed', 'closed', '', '52-autosave-v1', '', '', '2018-03-08 06:41:06', '2018-03-08 06:41:06', '', 52, 'http://localhost/office_projects/cycling/2018/03/08/52-autosave-v1/', 0, 'revision', '', 0),
(78, 1, '2018-03-08 06:47:18', '2018-03-08 06:47:18', '', 'intro-bg', '', 'inherit', 'open', 'closed', '', 'intro-bg', '', '', '2018-03-08 06:47:18', '2018-03-08 06:47:18', '', 52, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/intro-bg.jpg', 0, 'attachment', 'image/jpeg', 0),
(79, 1, '2018-03-08 06:47:30', '2018-03-08 06:47:30', 'Nagarkot is a rocking Mountain Trail Biking destination. We’ll take this to greater heights this time with our Himalayan biking adventure challenge with inspiring views of the himalayas. Blaze through the trails going uphill, downhill with villages, terraced farmlands and forests on the way.\r\n\r\nScenic uphill ride to the 360 view tower with an elevation gain of 200m from where you can enjoy the unparalleled Himalayan vistas, 5 out of 8 highest peaks of Nepal can been seen from here in a crystal clear day (Everest not to be forgotten) along with lots of 7000’s meter and 6000’s meter high peak. After the visit to the view tower, we will pass through Serene trail inside pine forest with occasional bird watching opportunity as well. (Please follow the map as it can be confusing for you). Then, the trail passes through different villages commanding unobstructed views of rolling hills, verdant valleys, lush forests, snowcapped himalayas.', 'Biking Trail', '', 'inherit', 'closed', 'closed', '', '52-revision-v1', '', '', '2018-03-08 06:47:30', '2018-03-08 06:47:30', '', 52, 'http://localhost/office_projects/cycling/2018/03/08/52-revision-v1/', 0, 'revision', '', 0),
(80, 1, '2018-03-08 06:56:00', '2018-03-08 06:56:00', 'a:7:{s:4:"type";s:3:"tab";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"placement";s:3:"top";s:8:"endpoint";i:0;}', 'Highlight Tab', '', 'publish', 'closed', 'closed', '', 'field_5aa0de515a4b7', '', '', '2018-03-08 08:04:13', '2018-03-08 08:04:13', '', 76, 'http://localhost/office_projects/cycling/?post_type=acf-field&#038;p=80', 1, 'acf-field', '', 0),
(81, 1, '2018-03-08 06:56:31', '2018-03-08 06:56:31', 'a:9:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;}', 'Highlight Content', 'highlight_content', 'publish', 'closed', 'closed', '', 'field_5aa0de851055f', '', '', '2018-03-08 08:04:13', '2018-03-08 08:04:13', '', 76, 'http://localhost/office_projects/cycling/?post_type=acf-field&#038;p=81', 2, 'acf-field', '', 0),
(82, 1, '2018-03-08 06:56:55', '2018-03-08 06:56:55', 'a:7:{s:4:"type";s:3:"tab";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"placement";s:3:"top";s:8:"endpoint";i:0;}', 'Detail Tab', '', 'publish', 'closed', 'closed', '', 'field_5aa0dea3f9269', '', '', '2018-03-08 08:04:13', '2018-03-08 08:04:13', '', 76, 'http://localhost/office_projects/cycling/?post_type=acf-field&#038;p=82', 3, 'acf-field', '', 0),
(83, 1, '2018-03-08 07:04:22', '2018-03-08 07:04:22', 'a:9:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;}', 'Detail Content', 'detail_content', 'publish', 'closed', 'closed', '', 'field_5aa0e00266336', '', '', '2018-03-08 08:04:13', '2018-03-08 08:04:13', '', 76, 'http://localhost/office_projects/cycling/?post_type=acf-field&#038;p=83', 4, 'acf-field', '', 0),
(84, 1, '2018-03-08 07:04:22', '2018-03-08 07:04:22', 'a:7:{s:4:"type";s:3:"tab";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"placement";s:3:"top";s:8:"endpoint";i:0;}', 'With Guide Tab', '', 'publish', 'closed', 'closed', '', 'field_5aa0e03d66337', '', '', '2018-03-08 08:04:13', '2018-03-08 08:04:13', '', 76, 'http://localhost/office_projects/cycling/?post_type=acf-field&#038;p=84', 5, 'acf-field', '', 0),
(85, 1, '2018-03-08 07:04:22', '2018-03-08 07:04:22', 'a:9:{s:4:"type";s:7:"wysiwyg";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:4:"tabs";s:3:"all";s:7:"toolbar";s:4:"full";s:12:"media_upload";i:1;}', 'With Guide Content', 'with_guide_content', 'publish', 'closed', 'closed', '', 'field_5aa0e06366338', '', '', '2018-03-08 08:04:13', '2018-03-08 08:04:13', '', 76, 'http://localhost/office_projects/cycling/?post_type=acf-field&#038;p=85', 6, 'acf-field', '', 0),
(86, 1, '2018-03-08 07:11:38', '2018-03-08 07:11:38', 'Nagarkot is a rocking Mountain Trail Biking destination. We’ll take this to greater heights this time with our Himalayan biking adventure challenge with inspiring views of the himalayas. Blaze through the trails going uphill, downhill with villages, terraced farmlands and forests on the way.\r\n\r\nScenic uphill ride to the 360 view tower with an elevation gain of 200m from where you can enjoy the unparalleled Himalayan vistas, 5 out of 8 highest peaks of Nepal can been seen from here in a crystal clear day (Everest not to be forgotten) along with lots of 7000’s meter and 6000’s meter high peak. After the visit to the view tower, we will pass through Serene trail inside pine forest with occasional bird watching opportunity as well. (Please follow the map as it can be confusing for you). Then, the trail passes through different villages commanding unobstructed views of rolling hills, verdant valleys, lush forests, snowcapped himalayas.', 'Biking Trail', '', 'inherit', 'closed', 'closed', '', '52-revision-v1', '', '', '2018-03-08 07:11:38', '2018-03-08 07:11:38', '', 52, 'http://localhost/office_projects/cycling/2018/03/08/52-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `web_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(87, 1, '2018-03-08 07:28:49', '2018-03-08 07:28:49', '<div class="row">\r\n   <div class="col-sm-12 form-group">\r\n    [select* noofpax id:noofrooms class:form-control "--Number Of People--" "1" "2" "3" "3+"]\r\n</div>\r\n\r\n<div class="col-sm-12 form-group">                 \r\n[text* name id:name class:form-control placeholder "Name"]\r\n</div>\r\n\r\n<div class="col-sm-12 form-group">\r\n[email* email id:email class:form-control placeholder "Email"]\r\n</div>\r\n\r\n<div class="col-sm-12 form-group">\r\n[number* contact_number id:contact_number class:form-control placeholder "Number"]\r\n</div>\r\n\r\n<div class="col-sm-12 form-group">\r\n[select* country id:country class:form-control "Select Your Country" "Australia" "Bhutan" "China " "India" "Japan" "USA" "Canada" "Bangladesh" "France" "UK" "Portugal" "Nepal" "Netherlands" "NewZealand" "Norway" "Pakistan" "Poland"]\r\n                 \r\n</div>\r\n \r\n<div class="clearfix"></div>\r\n                  \r\n <div class="col-sm-12 form-group">\r\n [submit id:contact_submit class:btn class:send-us class:hvr-fade "Submit"]\r\n </div>\r\n </div>\n1\nHimalaya Biking "[your-subject]"\n[your-name] <pracriti11neupane@gmail.com>\npracriti11neupane@gmail.com\nFrom: [your-name] <[your-email]>\r\nSubject: [your-subject]\r\n\r\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Himalaya Biking (http://localhost/office_projects/cycling)\nReply-To: [your-email]\n\n\n\n\nHimalaya Biking "[your-subject]"\nHimalaya Biking <pracriti11neupane@gmail.com>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Himalaya Biking (http://localhost/office_projects/cycling)\nReply-To: pracriti11neupane@gmail.com\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nYour entered code is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'Enquiry Form', '', 'publish', 'closed', 'closed', '', 'contact-form-1', '', '', '2018-03-08 07:57:42', '2018-03-08 07:57:42', '', 0, 'http://localhost/office_projects/cycling/?post_type=wpcf7_contact_form&#038;p=87', 0, 'wpcf7_contact_form', '', 0),
(88, 1, '2018-03-08 08:03:20', '2018-03-08 08:03:20', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Central Part Image', 'central_part_image', 'publish', 'closed', 'closed', '', 'field_5aa0eddad0dca', '', '', '2018-03-08 08:04:13', '2018-03-08 08:04:13', '', 76, 'http://localhost/office_projects/cycling/?post_type=acf-field&#038;p=88', 0, 'acf-field', '', 0),
(89, 1, '2018-03-08 08:05:27', '2018-03-08 08:05:27', '', 'banner5', '', 'inherit', 'open', 'closed', '', 'banner5-2', '', '', '2018-03-08 08:05:31', '2018-03-08 08:05:31', '', 52, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/banner5-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(90, 1, '2018-03-08 08:05:35', '2018-03-08 08:05:35', 'Nagarkot is a rocking Mountain Trail Biking destination. We’ll take this to greater heights this time with our Himalayan biking adventure challenge with inspiring views of the himalayas. Blaze through the trails going uphill, downhill with villages, terraced farmlands and forests on the way.\r\n\r\nScenic uphill ride to the 360 view tower with an elevation gain of 200m from where you can enjoy the unparalleled Himalayan vistas, 5 out of 8 highest peaks of Nepal can been seen from here in a crystal clear day (Everest not to be forgotten) along with lots of 7000’s meter and 6000’s meter high peak. After the visit to the view tower, we will pass through Serene trail inside pine forest with occasional bird watching opportunity as well. (Please follow the map as it can be confusing for you). Then, the trail passes through different villages commanding unobstructed views of rolling hills, verdant valleys, lush forests, snowcapped himalayas.', 'Biking Trail', '', 'inherit', 'closed', 'closed', '', '52-revision-v1', '', '', '2018-03-08 08:05:35', '2018-03-08 08:05:35', '', 52, 'http://localhost/office_projects/cycling/2018/03/08/52-revision-v1/', 0, 'revision', '', 0),
(91, 1, '2018-03-08 08:14:30', '2018-03-08 08:14:30', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:4:"page";s:8:"operator";s:2:"==";s:5:"value";s:2:"55";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";}', 'Gallery', 'gallery', 'publish', 'closed', 'closed', '', 'group_5aa0f0d3a135b', '', '', '2018-03-08 08:26:51', '2018-03-08 08:26:51', '', 0, 'http://localhost/office_projects/cycling/?post_type=acf-field-group&#038;p=91', 0, 'acf-field-group', '', 0),
(92, 1, '2018-03-08 08:15:29', '2018-03-08 08:15:29', 'a:10:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"collapsed";s:0:"";s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"layout";s:5:"table";s:12:"button_label";s:7:"Add Row";}', 'Gallery Files', 'gallery_files', 'publish', 'closed', 'closed', '', 'field_5aa0f0ebc2489', '', '', '2018-03-08 08:15:29', '2018-03-08 08:15:29', '', 91, 'http://localhost/office_projects/cycling/?post_type=acf-field&p=92', 0, 'acf-field', '', 0),
(93, 1, '2018-03-08 08:15:29', '2018-03-08 08:15:29', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Images', 'images', 'publish', 'closed', 'closed', '', 'field_5aa0f112c248a', '', '', '2018-03-08 08:26:51', '2018-03-08 08:26:51', '', 92, 'http://localhost/office_projects/cycling/?post_type=acf-field&#038;p=93', 0, 'acf-field', '', 0),
(94, 1, '2018-03-08 08:16:23', '2018-03-08 08:16:23', '', '23', '', 'inherit', 'open', 'closed', '', '23', '', '', '2018-03-08 08:16:23', '2018-03-08 08:16:23', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/23.jpeg', 0, 'attachment', 'image/jpeg', 0),
(95, 1, '2018-03-08 08:16:25', '2018-03-08 08:16:25', '', '22', '', 'inherit', 'open', 'closed', '', '22', '', '', '2018-03-08 08:16:25', '2018-03-08 08:16:25', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/22.jpeg', 0, 'attachment', 'image/jpeg', 0),
(96, 1, '2018-03-08 08:16:27', '2018-03-08 08:16:27', '', '21', '', 'inherit', 'open', 'closed', '', '21', '', '', '2018-03-08 08:16:27', '2018-03-08 08:16:27', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/21.jpeg', 0, 'attachment', 'image/jpeg', 0),
(97, 1, '2018-03-08 08:16:29', '2018-03-08 08:16:29', '', '20', '', 'inherit', 'open', 'closed', '', '20', '', '', '2018-03-08 08:16:29', '2018-03-08 08:16:29', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/20.jpeg', 0, 'attachment', 'image/jpeg', 0),
(98, 1, '2018-03-08 08:16:31', '2018-03-08 08:16:31', '', '19', '', 'inherit', 'open', 'closed', '', '19', '', '', '2018-03-08 08:16:31', '2018-03-08 08:16:31', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/19.jpeg', 0, 'attachment', 'image/jpeg', 0),
(99, 1, '2018-03-08 08:16:33', '2018-03-08 08:16:33', '', '18', '', 'inherit', 'open', 'closed', '', '18', '', '', '2018-03-08 08:16:33', '2018-03-08 08:16:33', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/18.jpeg', 0, 'attachment', 'image/jpeg', 0),
(100, 1, '2018-03-08 08:16:36', '2018-03-08 08:16:36', '', '17', '', 'inherit', 'open', 'closed', '', '17', '', '', '2018-03-08 08:16:36', '2018-03-08 08:16:36', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/17.jpeg', 0, 'attachment', 'image/jpeg', 0),
(101, 1, '2018-03-08 08:16:38', '2018-03-08 08:16:38', '', '16', '', 'inherit', 'open', 'closed', '', '16', '', '', '2018-03-08 08:16:38', '2018-03-08 08:16:38', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/16.jpeg', 0, 'attachment', 'image/jpeg', 0),
(102, 1, '2018-03-08 08:16:40', '2018-03-08 08:16:40', '', '15', '', 'inherit', 'open', 'closed', '', '15', '', '', '2018-03-08 08:16:40', '2018-03-08 08:16:40', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/15.jpeg', 0, 'attachment', 'image/jpeg', 0),
(103, 1, '2018-03-08 08:16:42', '2018-03-08 08:16:42', '', '14', '', 'inherit', 'open', 'closed', '', '14', '', '', '2018-03-08 08:16:42', '2018-03-08 08:16:42', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/14.jpeg', 0, 'attachment', 'image/jpeg', 0),
(104, 1, '2018-03-08 08:16:44', '2018-03-08 08:16:44', '', '13', '', 'inherit', 'open', 'closed', '', '13', '', '', '2018-03-08 08:16:44', '2018-03-08 08:16:44', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/13.jpeg', 0, 'attachment', 'image/jpeg', 0),
(105, 1, '2018-03-08 08:16:46', '2018-03-08 08:16:46', '', '12', '', 'inherit', 'open', 'closed', '', '12', '', '', '2018-03-08 08:16:46', '2018-03-08 08:16:46', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/12.jpeg', 0, 'attachment', 'image/jpeg', 0),
(106, 1, '2018-03-08 08:16:48', '2018-03-08 08:16:48', '', '11', '', 'inherit', 'open', 'closed', '', '11-2', '', '', '2018-03-08 08:16:48', '2018-03-08 08:16:48', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/11.jpeg', 0, 'attachment', 'image/jpeg', 0),
(107, 1, '2018-03-08 08:16:50', '2018-03-08 08:16:50', '', '10', '', 'inherit', 'open', 'closed', '', '10', '', '', '2018-03-08 08:16:50', '2018-03-08 08:16:50', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/10.jpeg', 0, 'attachment', 'image/jpeg', 0),
(108, 1, '2018-03-08 08:16:53', '2018-03-08 08:16:53', '', '9', '', 'inherit', 'open', 'closed', '', '9', '', '', '2018-03-08 08:16:53', '2018-03-08 08:16:53', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/9.jpeg', 0, 'attachment', 'image/jpeg', 0),
(109, 1, '2018-03-08 08:16:55', '2018-03-08 08:16:55', '', '8', '', 'inherit', 'open', 'closed', '', '8', '', '', '2018-03-08 08:16:55', '2018-03-08 08:16:55', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/8.jpeg', 0, 'attachment', 'image/jpeg', 0),
(110, 1, '2018-03-08 08:16:57', '2018-03-08 08:16:57', '', '7', '', 'inherit', 'open', 'closed', '', '7', '', '', '2018-03-08 08:16:57', '2018-03-08 08:16:57', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/7.jpeg', 0, 'attachment', 'image/jpeg', 0),
(111, 1, '2018-03-08 08:16:59', '2018-03-08 08:16:59', '', '6', '', 'inherit', 'open', 'closed', '', '6', '', '', '2018-03-08 08:16:59', '2018-03-08 08:16:59', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/6.jpg', 0, 'attachment', 'image/jpeg', 0),
(112, 1, '2018-03-08 08:18:20', '2018-03-08 08:18:20', '', '23', '', 'inherit', 'open', 'closed', '', '23-2', '', '', '2018-03-08 08:19:58', '2018-03-08 08:19:58', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/23-1.jpeg', 0, 'attachment', 'image/jpeg', 0),
(113, 1, '2018-03-08 08:18:22', '2018-03-08 08:18:22', '', '22', '', 'inherit', 'open', 'closed', '', '22-2', '', '', '2018-03-08 08:18:22', '2018-03-08 08:18:22', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/22-1.jpeg', 0, 'attachment', 'image/jpeg', 0),
(114, 1, '2018-03-08 08:18:24', '2018-03-08 08:18:24', '', '21', '', 'inherit', 'open', 'closed', '', '21-2', '', '', '2018-03-08 08:18:24', '2018-03-08 08:18:24', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/21-1.jpeg', 0, 'attachment', 'image/jpeg', 0),
(115, 1, '2018-03-08 08:18:26', '2018-03-08 08:18:26', '', '20', '', 'inherit', 'open', 'closed', '', '20-2', '', '', '2018-03-08 08:18:26', '2018-03-08 08:18:26', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/20-1.jpeg', 0, 'attachment', 'image/jpeg', 0),
(116, 1, '2018-03-08 08:18:28', '2018-03-08 08:18:28', '', '19', '', 'inherit', 'open', 'closed', '', '19-2', '', '', '2018-03-08 08:18:28', '2018-03-08 08:18:28', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/19-1.jpeg', 0, 'attachment', 'image/jpeg', 0),
(117, 1, '2018-03-08 08:18:30', '2018-03-08 08:18:30', '', '18', '', 'inherit', 'open', 'closed', '', '18-2', '', '', '2018-03-08 08:18:30', '2018-03-08 08:18:30', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/18-1.jpeg', 0, 'attachment', 'image/jpeg', 0),
(118, 1, '2018-03-08 08:18:32', '2018-03-08 08:18:32', '', '17', '', 'inherit', 'open', 'closed', '', '17-2', '', '', '2018-03-08 08:18:32', '2018-03-08 08:18:32', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/17-1.jpeg', 0, 'attachment', 'image/jpeg', 0),
(119, 1, '2018-03-08 08:18:34', '2018-03-08 08:18:34', '', '16', '', 'inherit', 'open', 'closed', '', '16-2', '', '', '2018-03-08 08:18:34', '2018-03-08 08:18:34', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/16-1.jpeg', 0, 'attachment', 'image/jpeg', 0),
(120, 1, '2018-03-08 08:18:37', '2018-03-08 08:18:37', '', '15', '', 'inherit', 'open', 'closed', '', '15-2', '', '', '2018-03-08 08:18:37', '2018-03-08 08:18:37', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/15-1.jpeg', 0, 'attachment', 'image/jpeg', 0),
(121, 1, '2018-03-08 08:18:38', '2018-03-08 08:18:38', '', '14', '', 'inherit', 'open', 'closed', '', '14-2', '', '', '2018-03-08 08:18:38', '2018-03-08 08:18:38', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/14-1.jpeg', 0, 'attachment', 'image/jpeg', 0),
(122, 1, '2018-03-08 08:18:40', '2018-03-08 08:18:40', '', '13', '', 'inherit', 'open', 'closed', '', '13-2', '', '', '2018-03-08 08:18:40', '2018-03-08 08:18:40', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/13-1.jpeg', 0, 'attachment', 'image/jpeg', 0),
(123, 1, '2018-03-08 08:18:43', '2018-03-08 08:18:43', '', '12', '', 'inherit', 'open', 'closed', '', '12-2', '', '', '2018-03-08 08:18:43', '2018-03-08 08:18:43', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/12-1.jpeg', 0, 'attachment', 'image/jpeg', 0),
(124, 1, '2018-03-08 08:18:44', '2018-03-08 08:18:44', '', '11', '', 'inherit', 'open', 'closed', '', '11-3', '', '', '2018-03-08 08:18:44', '2018-03-08 08:18:44', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/11-1.jpeg', 0, 'attachment', 'image/jpeg', 0),
(125, 1, '2018-03-08 08:18:46', '2018-03-08 08:18:46', '', '10', '', 'inherit', 'open', 'closed', '', '10-2', '', '', '2018-03-08 08:18:46', '2018-03-08 08:18:46', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/10-1.jpeg', 0, 'attachment', 'image/jpeg', 0),
(126, 1, '2018-03-08 08:18:48', '2018-03-08 08:18:48', '', '9', '', 'inherit', 'open', 'closed', '', '9-2', '', '', '2018-03-08 08:18:48', '2018-03-08 08:18:48', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/9-1.jpeg', 0, 'attachment', 'image/jpeg', 0),
(127, 1, '2018-03-08 08:18:50', '2018-03-08 08:18:50', '', '8', '', 'inherit', 'open', 'closed', '', '8-2', '', '', '2018-03-08 08:18:50', '2018-03-08 08:18:50', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/8-1.jpeg', 0, 'attachment', 'image/jpeg', 0),
(128, 1, '2018-03-08 08:18:53', '2018-03-08 08:18:53', '', '7', '', 'inherit', 'open', 'closed', '', '7-2', '', '', '2018-03-08 08:18:53', '2018-03-08 08:18:53', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/7-1.jpeg', 0, 'attachment', 'image/jpeg', 0),
(129, 1, '2018-03-08 08:18:57', '2018-03-08 08:18:57', '', '6', '', 'inherit', 'open', 'closed', '', '6-2', '', '', '2018-03-08 08:18:57', '2018-03-08 08:18:57', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/6-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(130, 1, '2018-03-08 08:19:09', '2018-03-08 08:19:09', '', '5', '', 'inherit', 'open', 'closed', '', '5', '', '', '2018-03-08 08:19:09', '2018-03-08 08:19:09', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/5.jpg', 0, 'attachment', 'image/jpeg', 0),
(131, 1, '2018-03-08 08:19:22', '2018-03-08 08:19:22', '', '4', '', 'inherit', 'open', 'closed', '', '4', '', '', '2018-03-08 08:19:22', '2018-03-08 08:19:22', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/4.jpg', 0, 'attachment', 'image/jpeg', 0),
(132, 1, '2018-03-08 08:19:36', '2018-03-08 08:19:36', '', '3', '', 'inherit', 'open', 'closed', '', '3', '', '', '2018-03-08 08:19:36', '2018-03-08 08:19:36', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/3.jpg', 0, 'attachment', 'image/jpeg', 0),
(133, 1, '2018-03-08 08:19:49', '2018-03-08 08:19:49', '', '2', '', 'inherit', 'open', 'closed', '', '2', '', '', '2018-03-08 08:19:49', '2018-03-08 08:19:49', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/2.jpeg', 0, 'attachment', 'image/jpeg', 0),
(134, 1, '2018-03-08 08:19:51', '2018-03-08 08:19:51', '', '1', '', 'inherit', 'open', 'closed', '', '1', '', '', '2018-03-08 08:19:51', '2018-03-08 08:19:51', '', 55, 'http://localhost/office_projects/cycling/wp-content/uploads/2018/03/1.jpeg', 0, 'attachment', 'image/jpeg', 0),
(135, 1, '2018-03-08 08:20:07', '2018-03-08 08:20:07', '', 'Gallery', '', 'inherit', 'closed', 'closed', '', '55-revision-v1', '', '', '2018-03-08 08:20:07', '2018-03-08 08:20:07', '', 55, 'http://localhost/office_projects/cycling/2018/03/08/55-revision-v1/', 0, 'revision', '', 0),
(136, 1, '2018-03-08 08:45:22', '2018-03-08 08:45:22', '<div class="col-md-6">\r\n  <label>Full Name :</label>\r\n [text* name id:name class:form-control]\r\n  </div>\r\n           \r\n  <div class="col-md-6">\r\n    <label>Email :</label>\r\n   [email* email id:email class:form-control]\r\n   </div>\r\n                \r\n<div class="col-md-6">\r\n <label>Phone :</label>\r\n [number* number-551 id:phone class:form-control]\r\n </div>\r\n                \r\n<div class="col-md-6">\r\n<label>Country/City :</label>\r\n[text* country id:country class:form-control]\r\n</div>\r\n                \r\n<div class="clearfix"></div>\r\n <div class="col-sm-12">\r\n <label>Message :</label>\r\n [textarea* comment id:comment class:form-control]\r\n  </div>\r\n                \r\n<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-btm-1">\r\n <i class="fa fa-paper-plane" aria-hidden="true"></i>\r\n </button>\r\n </div>\n1\nHimalaya Biking "[your-subject]"\n[your-name] <pracriti11neupane@gmail.com>\npracriti11neupane@gmail.com\nFrom: [your-name] <[your-email]>\r\nSubject: [your-subject]\r\n\r\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Himalaya Biking (http://localhost/office_projects/cycling)\nReply-To: [your-email]\n\n\n\n\nHimalaya Biking "[your-subject]"\nHimalaya Biking <pracriti11neupane@gmail.com>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Himalaya Biking (http://localhost/office_projects/cycling)\nReply-To: pracriti11neupane@gmail.com\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nYour entered code is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'Contact Form', '', 'publish', 'closed', 'closed', '', 'contact-form', '', '', '2018-03-08 09:07:08', '2018-03-08 09:07:08', '', 0, 'http://localhost/office_projects/cycling/?post_type=wpcf7_contact_form&#038;p=136', 0, 'wpcf7_contact_form', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `web_termmeta`
--

CREATE TABLE `web_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `web_terms`
--

CREATE TABLE `web_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `web_terms`
--

INSERT INTO `web_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Primary Menu', 'primary-menu', 0);

-- --------------------------------------------------------

--
-- Table structure for table `web_term_relationships`
--

CREATE TABLE `web_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `web_term_relationships`
--

INSERT INTO `web_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(11, 2, 0),
(61, 2, 0),
(62, 2, 0),
(63, 2, 0),
(64, 2, 0),
(65, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `web_term_taxonomy`
--

CREATE TABLE `web_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `web_term_taxonomy`
--

INSERT INTO `web_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'nav_menu', '', 0, 6);

-- --------------------------------------------------------

--
-- Table structure for table `web_usermeta`
--

CREATE TABLE `web_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `web_usermeta`
--

INSERT INTO `web_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'biking'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'web_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(13, 1, 'web_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:2:{s:64:"151f977944d940fed50ab7dee956174de923a739acf3fd94bd126b3f7c913c25";a:4:{s:10:"expiration";i:1520588083;s:2:"ip";s:3:"::1";s:2:"ua";s:135:"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/64.0.3282.167 Chrome/64.0.3282.167 Safari/537.36";s:5:"login";i:1520415283;}s:64:"85b7e93206c11d18cff6910e99d51a92c1e448fe576ae46258fd3d6862848f94";a:4:{s:10:"expiration";i:1520658404;s:2:"ip";s:3:"::1";s:2:"ua";s:135:"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/64.0.3282.167 Chrome/64.0.3282.167 Safari/537.36";s:5:"login";i:1520485604;}}'),
(17, 1, 'web_dashboard_quick_press_last_post_id', '3'),
(18, 1, 'community-events-location', 'a:1:{s:2:"ip";s:2:"::";}'),
(19, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:"link-target";i:1;s:11:"css-classes";i:2;s:3:"xfn";i:3;s:11:"description";i:4;s:15:"title-attribute";}'),
(20, 1, 'metaboxhidden_nav-menus', 'a:1:{i:0;s:12:"add-post_tag";}'),
(21, 1, 'acf_user_settings', 'a:0:{}'),
(22, 1, 'web_user-settings', 'libraryContent=browse&editor=html'),
(23, 1, 'web_user-settings-time', '1520493451'),
(24, 1, 'closedpostboxes_toplevel_page_theme-general-settings', 'a:1:{i:0;s:23:"acf-group_5a9fcfcd37e89";}'),
(25, 1, 'metaboxhidden_toplevel_page_theme-general-settings', 'a:0:{}'),
(26, 1, 'nav_menu_recently_edited', '2');

-- --------------------------------------------------------

--
-- Table structure for table `web_users`
--

CREATE TABLE `web_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `web_users`
--

INSERT INTO `web_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'biking', '$P$BMLTbvXuqsb04Y.ApdRoHXYuCDp55Z1', 'biking', 'pracriti11neupane@gmail.com', '', '2018-03-07 08:33:37', '', 0, 'biking');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `web_commentmeta`
--
ALTER TABLE `web_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `web_comments`
--
ALTER TABLE `web_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `web_links`
--
ALTER TABLE `web_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `web_options`
--
ALTER TABLE `web_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `web_postmeta`
--
ALTER TABLE `web_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `web_posts`
--
ALTER TABLE `web_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `web_termmeta`
--
ALTER TABLE `web_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `web_terms`
--
ALTER TABLE `web_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `web_term_relationships`
--
ALTER TABLE `web_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `web_term_taxonomy`
--
ALTER TABLE `web_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `web_usermeta`
--
ALTER TABLE `web_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `web_users`
--
ALTER TABLE `web_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `web_commentmeta`
--
ALTER TABLE `web_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `web_comments`
--
ALTER TABLE `web_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `web_links`
--
ALTER TABLE `web_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `web_options`
--
ALTER TABLE `web_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=225;
--
-- AUTO_INCREMENT for table `web_postmeta`
--
ALTER TABLE `web_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=705;
--
-- AUTO_INCREMENT for table `web_posts`
--
ALTER TABLE `web_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;
--
-- AUTO_INCREMENT for table `web_termmeta`
--
ALTER TABLE `web_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `web_terms`
--
ALTER TABLE `web_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `web_term_taxonomy`
--
ALTER TABLE `web_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `web_usermeta`
--
ALTER TABLE `web_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `web_users`
--
ALTER TABLE `web_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
