<?php
/**
*Template Name: About Page
*/
 get_header(); 
if (have_posts()):
  while (have_posts()): the_post(); 
$menuSelected = 'about-us'; ?>
 <?php 
 $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
<section class="inner-banner about" style= "background: url('<?php echo $backgroundImg[0]; ?>')no-repeat fixed center top / cover;">
  <div class="overlay">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="main-heading">
            <h1 class="decorated"><span>About Us</span></h1>
          </div>
          
        </div>
        
      </div>
      <div class="row">
        <div class="col-md-8">
          <div class="rightColumn">
            <div class="mt-content">
              <?php the_content(); ?>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="abt-img">
           <img src=" <?php the_field('about_sidebar_image', option); ?>" alt="">
          </div>
          
        </div>
      </div>
    </section>
    <secttion id="about-bottom">
    </secttion>
    <?php endwhile;
else:
echo '<p>No content found</p>';
endif;
get_footer();
 ?>