<?php
/* Template Name: video */
get_header();
if (have_posts()):
	while (have_posts()): the_post(); 
		?>
		<section class="inner-banner gal">
			<div class="overlay">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="main-heading">
								<h1 class="decorated"><span>Our Videos</span></h1>
							</div>
						</div>

						<div class="gallery-panel">
							<?php 
							if(have_rows('video_files')):
								while(have_rows('video_files')): the_row();
									?>
									<div class="col-xs-6 col-sm-4">
										<button onclick="changeVideo('<?php the_sub_field('video_id'); ?>')"><img src="http://img.youtube.com/vi/<?php the_sub_field('video_id'); ?>/mqdefault.jpg" alt="Video" style="height: 306px"></button>
										<!-- Modal -->
										<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
													<iframe id="iframeYoutube" width="100%" height="400px" src="<?php the_sub_field('video_links'); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
													<button type="button" class="btn btn-default modal-btn" data-dismiss="modal"><i class="fa fa-close"></i></button>
												</div>
											</div>
										</div>
									</div>
									<?php 
								endwhile;
							endif;
							?>
						</div>
					</div>
				</div>
			</div>
		</section>
		<?php
	endwhile;
else:
	echo '<p>No content found</p>';
endif;
get_footer();
?>