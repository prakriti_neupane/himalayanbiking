<?php
/**
*Template Name: Contact Page
*/
 get_header(); 
if (have_posts()):
  while (have_posts()): the_post(); 

?>
<section class="inner-banner inv">
  <div class="overlay">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          
          <div class="main-heading">
               <h1 class="each-word">Get In Touch</h1>
            <p class="slogan">If you have any question or require assistance, please complete the Contact us form.</p>
          </div>
          

        </div>
        <div class="contact-box">
          <div class="col-md-7 col-md-offset-1">
      
              <form role= "form" novalidate id="contact_form" method="post">
                <h3>Send Us A Message</h3>


                <?php echo do_shortcode('[contact-form-7 id="136" title="Contact Form"]'); ?>
              </form>
        
          </div>
          <div class="col-md-3">
            <div class="contact-details">
              <h3>Contact Info</h3>
              <div class="text-holder"> <i class="fa fa-phone color"></i>
                
                
                <span><?php the_field('phone_number', option) ?></span></p>
                
              </div>
              <div class="text-holder"> <i class="fa fa-envelope color"></i>
                
                
                <span><?php the_field('email', option) ?></span></p>
                </span></p>
                
              </div>
              <div class="text-holder"> <i class="fa fa-map-marker color"></i>
                
                <span><?php the_field('address', option) ?>
                </span></p>
              </div>
              
            </div>
          </div>
          
       
        </div>
 
      </div>

    </div>
  </div>
</section>

<?php endwhile;
else:
echo '<p>No content found</p>';
endif;
get_footer();
 ?>

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
 