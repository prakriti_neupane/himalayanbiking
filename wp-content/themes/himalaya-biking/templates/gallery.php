<?php
/**
*Template Name: Gallery Page
*/
 get_header(); 
if (have_posts()):
  while (have_posts()): the_post(); 

?>
<section class="inner-banner gal">
  <div class="overlay">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
           <div class="main-heading">
            <h1 class="decorated"><span>The Gallery</span></h1>
          </div>
        </div>
    
        <div class="gallery-panel">
          <?php 
           if(have_rows('gallery_files')):
            while(have_rows('gallery_files')): the_row();
          $gallery= get_sub_field('images');  ?>

          <div class="col-xs-6 col-sm-4">
            <a href="#" class="thumbnail" data-toggle="modal" data-target="#lightbox">
              <img src="<?php echo  $gallery['url']; ?>" alt="<?php echo  $gallery['alt']; ?>" />
            </a>
          </div>
          <!--lightbox -->
          <div id="lightbox" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <div class="modal-content">
                <div class="modal-body">
                  <img src="" alt="" />
                </div>
              </div>
            </div>
          </div>
          <?php 
      endwhile;
    endif;
        ?>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Footer Section Starts Here -->
<?php endwhile;
else:
echo '<p>No content found</p>';
endif;
get_footer();
 ?>
