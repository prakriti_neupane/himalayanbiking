<?php
/**
*Template Name: Biking Trial Page
*/
get_header(); 
if (have_posts()):
  while (have_posts()): the_post(); 
    ?>
    <?php
    $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
    ?>
    <section class="inner-banner gal"  style="background: rgba(0, 0, 0, 0) url('<?php echo $backgroundImg[0]; ?>') no-repeat fixed center top / cover ; padding-bottom: 160px;">
      <div class="overlay">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="main-heading">
                <h1 class="decorated"><span>Event</span></h1>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-9">
              <div class="rightColumn">
                <div class="mt-content">
                  <h2>Himalayan biking adventure in Nagarkot </h2>
                  <div class="hero-image">
                    <?php
                    $center_image= get_field('central_part_image'); 
                    ?>
                    <img src="<?php echo $center_image['url']; ?>" alt="<?php echo  $center_image['alt']; ?>" />
                  </div>
                  <div class="mt-package-cnt">
                    <div class="mt-content intro-content">
                      <?php the_content(); ?>
                    </div>

                    <div class="mt-content">
                      <div class="tabbable-panel">
                        <div class="tabbable-line">
                          <ul class="nav nav-tabs ">
                            <li class="active"><a href="#tab_default_1" data-toggle="tab">Highlights </a></li>
                            <li><a href="#tab_default_2" data-toggle="tab">Details</a></li>
                            <li><a href="#tab_default_3" data-toggle="tab">With Guide</a></li>
                            <li><a href="#tab_default_4" data-toggle="tab">Without Guide</a></li>
                            <li><a href="#tab_default_5" data-toggle="tab">Rent a Bike</a></li>
                          </ul>
                          <div class="tab-content">
                            <div class="tab-pane active" id="tab_default_1">
                              <h4>Highlights</h4>
                              <?php the_field('highlight_content'); ?>

                            </div>
                            <div class="tab-pane" id="tab_default_2">
                              <h4>Details</h4>
                              <?php the_field('detail_content'); ?>
                            </div>
                            <div class="tab-pane" id="tab_default_3">
                              <h4>With Guide</h4>
                              <?php the_field('with_guide_content'); ?>
                            </div>

                            <div class="tab-pane" id="tab_default_4">
                              <h4>Without Guide</h4>
                              <?php the_field('without_guide_detail'); ?>
                            </div>

                            <div class="tab-pane" id="tab_default_5">
                              <h4>Rent a Bike</h4>
                              <?php the_field('rent_details'); ?>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="mt-content guided">
                      <h3> Guided Tour</h3>
                      <?php
                      if(have_rows('guided_tours')):
                        while(have_rows('guided_tours')):
                          the_row();
                          ?>
                          <p><i class="fa fa-angle-right"></i><?php the_sub_field('tour_lists'); ?></p>
                          <?php
                        endwhile;
                      endif;
                      ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="mt-content mt-pkg">
                <h3>What we provide</h3>
                <p class="mt-brand"><strong>Cycle brand: </strong><?php the_field('cycle_brand'); ?></p>
                <p class="mt-specification"><strong>Specification: </strong><?php the_field('specification'); ?></p>
                <p class="mt-time"><strong><?php the_field('day'); ?></strong></p>
                <p class="mt-included"><strong>What is Included</strong><br><?php the_field('whats_included'); ?></p>
                <p class="price"><?php the_field('price'); ?></p>
              </div>
              <div class="mt-enquiry">
                <h3>Make an enquiry</h3>
                <form method="POST" role= "form">
                 <?php echo do_shortcode('[contact-form-7 id="87" title="Enquiry Form"]'); ?>
               </form>
             </div>
           </div>
         </div>
       </div>
     </div>
   </section>
 <?php endwhile;
else:
  echo '<p>No content found</p>';
endif;
get_footer();
?>