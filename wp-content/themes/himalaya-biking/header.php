<!DOCTYPE html>
<html lang="en-us">
  <head>
    <title>Himalayan Biking</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.png" type="image/x-icon">
    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.png" type="image/x-icon">
    <!-- Bootstrap -->
    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css">
    <link href="<?php echo get_template_directory_uri(); ?>/css/flexslider.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/media.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/slick.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/slick-theme.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/typist-style.css">

 </head>
  <?php
  $homepage = "/himalayanbiking/";
  $currentpage = $_SERVER['REQUEST_URI'];
 if($homepage == $currentpage): ?>
        <body>
          <?php else: ?>
            <body class="inner-page">
              <?php endif; ?>
      <!-- Header Sections start -->
      <div class="bg-holder">
        <header id="main_header" class="clearfix">
          <section id="pav-mainnav">
            <div class="container">
              <div class="row">
                <div class=" col-sm-12">
                  <nav class="navbar">
                    <div class="navbar-header">
                      <div class="navbar-collapse collapse">
                        <a class="logo" href="<?php echo home_url(); ?>"> <img src="<?php the_field('header_logo', option); ?>"/> </a> </h1>
                        <ul class="nav navbar-nav">
                          <?php 
                             wp_nav_menu(array(
                        'theme_location'=> 'primary',
                         'menu_id'=>'Primary Menu',
                         'container'=> false,
                         'menu_class'=> 'nav navbar-nav',
                         'fallback_cb'=> 'wp_bootstrap_navwalker::fallback',
                      'walker'=> new wp_bootstrap_navwalker()
                          ));
                          ?>
                        </ul>
                      </div>
                    </div>
                  </nav>
                  <div class="menu-toggle menu-open">
                    <div class="menu-toggle-hamburger"><span></span></div>
                  </div>
                  <div class="menu-wrap">
                    <div class="menu-overlay menu-close"></div>
                    <nav class="menu">
                      <h4>Menu</h4>
                      <ul class="main-menu">
                        
                        <li class="parent dropdown home-col"> <a href="index.php" <?php if(@$menuSelected == 'home') echo 'class="active"'; ?>>Home</a> </li>
                        <li class="full-width parent"> <a href="about-us.php" <?php if(@$menuSelected == 'about-us') echo 'class="active"'; ?>>About Us</a></li>
                        <li class="parent"> <a href="biking-trail.php" <?php if(@$menuSelected == 'biking-trail') echo 'class="active"'; ?>>Biking Trail</a> </li>
                        
                        <li> <a href="gallery.php" <?php if(@$menuSelected == 'gallery') echo 'class="active"'; ?>>Gallery</a> </li>
                        <li> <a href="events.php" <?php if(@$menuSelected == 'events') echo 'class="active"'; ?>>Events</a> </li>
                        <li class="last-child"> <a href="involved.php" <?php if(@$menuSelected == 'involved') echo 'class="active"'; ?>>Get Involved</a> </li>
                      </ul>
                      <div class="menu-toggle on">
                        <div class="menu-toggle-hamburger menu-close"><span></span></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </header>


