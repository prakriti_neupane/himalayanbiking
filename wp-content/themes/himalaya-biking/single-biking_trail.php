<?php get_header();
if(have_posts()):
	while(have_posts()):
		the_post();
?>
<section id="inner-banner" class="mtb1">
	<div class="container">
		<div class="row">
			
			<div class="col-md-12">
				<h1 class="inner-main-title"><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</section>
<section id="overview-section">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="mtb-content">
					<h2>Overview</h2>
					<?php the_content(); ?>
				</div>
				<div class="overview-detail">
					<ul>
						<li><p class="mt-time">Duration<span><?php the_field('duration'); ?></span></p></li>
						<li><p class="mt-group">Group Size<span><?php the_field('group_size'); ?></span></p></li>
						<li><p class="mt-difficulty">Dfficulty Level<span><?php the_field('difficulty_level'); ?></span></p></li>
					</ul>
				</div>
				
			</div>
			<div class="col-md-3 col-md-offset-1">
				<div class="mt-offer">
					<p>Starting From</p>
					<p class="mt-currency">USD</p>
					<h2 class="mt-price">314</h2>
					
				</div>
			</div>
		</div>
	</div>
</section>
<section id="mt-image-section" class="mt-1">
</section>
<section id="overview-section">
	<div class="container">
		<div class="row">
			<div class="col-md-7">
				<div class="mtb-content">
					<h2>Overview</h2>
					<div class="tabbable-panel">
						<div class="tabbable-line">
							<ul class="nav nav-tabs ">
								<li class="active"><a href="#tab_default_1" data-toggle="tab">Day 1 </a></li>
								<li class=""><a href="#tab_default_2" data-toggle="tab">Day 2</a></li>
								<li class=""><a href="#tab_default_3" data-toggle="tab">Day 3</a></li>
								<li class=""><a href="#tab_default_4" data-toggle="tab">Day 4</a></li>
								<li class=""><a href="#tab_default_5" data-toggle="tab">Day 5</a></li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane active" id="tab_default_1">
									<?php the_field('day1_content') ?>
									
								</div>
								<div class="tab-pane" id="tab_default_2">
									<?php the_field('day2_content') ?>
								</div>
								<div class="tab-pane" id="tab_default_3">
									<?php the_field('day3_content') ?>
								</div>

								<div class="tab-pane" id="tab_default_4">
									<?php the_field('day_4_content') ?>
								</div>

								
								<div class="tab-pane" id="tab_default_5">
									<?php the_field('day_5_content') ?>
								</div>




							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-5">
				<div class="price-details">
					<h2 class="sub-title">Price Details</h2>
					<div class="price trip-note-box">
						
						<div class="border-accentColor2 padding-15">
							<table class="table border-0 mt-table">
								<tbody><tr>
									<td class="font-header uppercase">Group Size</td>
									<td class="font-header uppercase">Price per person</td>
								</tr>
								<?php
								if(have_rows('price_details')):
									while(have_rows('price_details')): the_row();
								  ?>
								<tr>
									<td class="font-header"><?php the_sub_field('group_size'); ?></td>
									<td class="font-header"><?php the_sub_field('price_per_person'); ?></td>
								</tr>
								<?php
							endwhile;
						endif;
								 ?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="price-details">
					<h2 class="sub-title">Whats Included</h2>
					<ul class="mt-lists">
						<?php the_field('whats_included'); ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="mt-image-section" class="mt-2">
</section>
<section id="overview-section">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="mtb-content">
					<h2>Important Trip Info</h2>
					<div class="tabbable-panel">
						<div class="tabbable-line">
							<ul class="nav nav-tabs  col-md-2">
								<li class="active"><a href="#tabs_default_1" data-toggle="tab">Transportation </a></li>
								<li class=""><a href="#tabs_default_2" data-toggle="tab">Accomodation</a></li>
								<li class=""><a href="#tabs_default_3" data-toggle="tab">Meals</a></li>
								<li class=""><a href="#tabs_default_4" data-toggle="tab">Altitude</a></li>
								<li class=""><a href="#tabs_default_5" data-toggle="tab">Insurance</a></li>
							</ul>
							<div class="tab-content col-md-9 mt-info">
								<div class="tab-pane active" id="tabs_default_1">
									<?php the_field('transportation_details'); ?>
									
								</div>
								<div class="tab-pane" id="tabs_default_2">
									<?php the_field('accomodation_details'); ?>
								</div>
								<div class="tab-pane" id="tabs_default_3">
									<?php the_field('meals_details'); ?>
								</div>

								<div class="tab-pane" id="tabs_default_4">
									<?php the_field('altitude_detail'); ?>
								</div>

								<div class="tab-pane" id="tabs_default_5">
									<?php the_field('insurance_detail'); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="cmt-trip">
					<h2 class="sub-title">Customized Trip</h2>
					
					<p><?php the_field('customized_trip'); ?></p>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="mt-image-section" class="mt-3">
</section>
<?php 
endwhile;
endif;
get_footer();
?>