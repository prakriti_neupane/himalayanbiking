<?php
get_header();
if (have_posts()):
while (have_posts()): the_post();
?>
<!-- Banner Section Start -->
<section id="billboard">
  <div class="slider_img">
    <div class="flexslider">
      <ul class="slides">
        <?php
        if(have_rows('slider')):
        while(have_rows('slider')): the_row();
        $image = get_sub_field('slider_image');
        ?>
        <li>
          <a href="#"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
        </li>
        <?php
        endwhile;
        endif;
        ?>
      </ul>
      <ul class="flex-direction-nav"><li class="flex-nav-prev"><a href="#" class="flex-prev">Previous</a></li><li class="flex-nav-next"><a href="#" class="flex-next flex-disabled" tabindex="-1">Next</a></li></ul>
      <div class="banner-txt">
        <h1 class="decorated"><span>Never Stop</span></h1>
        <h1 class="lg-font">Exploring</h1>
      </div>
    </div>
  </div>
</section>
</div>
<!-- 3 icon section starts here -->
<section id="intro-icons">
<div class="container">
  <div class="row">
    <div class="col-md-4">
      <div class="ft-section">
        <figure class="mt-icons">
          <img src="<?php echo get_template_directory_uri();?>/images/icon2.png" alt="icon">
        </figure>
        <h4>Mountain biking for families and children.</h4>
      </div>
    </div>
    <div class="col-md-4">
      <div class="ft-section">
        <figure class="mt-icons">
          <img src="<?php echo get_template_directory_uri();?>/images/icon.png" alt="icon">
        </figure>
        <h4>Cross country downhill single tracks.</h4>
      </div>
    </div>
    <div class="col-md-4">
      <div class="ft-section">
        <figure class="mt-icons">
          <img src="<?php echo get_template_directory_uri();?>/images/icon3.png" alt="icon">
        </figure>
        <h4> Half day and full day mountain biking in beautiful trails of nagarkot</h4>
      </div>
    </div>
  </div>
</div>
</section>
<!-- intro video section starts here -->
<div id="intro-section">
<div class="container">
  <div class="row">
    <div class="col-md-7">
      <?php the_content(); ?>
      <a href="<?php echo get_site_url(); ?>/events">Know More About Our Events</a>
    </div>
    <div class="col-md-5">
      <?php
      $thumbnail= get_field('video_image');
      ?>
      <button onclick="changeVideo('<?php the_field('video_code'); ?>')"><img src="<?php echo $thumbnail['url']; ?>" alt="<?php echo $thumbnail['alt']; ?>" /></button>
      <!-- <div id="video-container">
        <iframe width="100%" height="315" src="https://www.youtube.com/embed/O3nn5b6G3Js?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
      </div> -->
    </div>
  </div>
</div>
</div>
<!--intro-section-stars-here -->
<div id="mt-cycle">
<div class="contents">
  <div class="variable slider">
    <?php
    if(have_rows('below_biking_adventure')):
    while(have_rows('below_biking_adventure')): the_row();
    $image1 = get_sub_field('images');
    ?>
    <div>
      <img src="<?php echo $image1['url']; ?>" alt="<?php echo $image1['alt']; ?>" />
      <div class="pdt-description">
        <p class="pdt-title"><?php the_sub_field('image_text'); ?></p>
        
      </div>
    </div>
    <?php
    endwhile;
    endif;
    ?>
  </div>
  
</div>
<div class="slider-section pull-left">
  <?php
  $side_image= get_field('sidebar_image_below_biking_adventure');
  ?>
  <li><img src="<?php echo  $side_image['url']; ?>" alt="<?php echo $side_image['alt']; ?>" /></li>
</ul>
</div>
<div class="clearfix"></div>
<!-- news and event section -->
<section id="event-section">
<?php
$backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
<div class="cyclist" style="background: url('<?php echo $backgroundImg[0]; ?>') no-repeat right bottom; height: 500px; width: 1700px; position: absolute; right:0; top:650px;">
  
</div>
<div class="container">
  <div class="row">
    <div class="col-md-6">
      <?php the_field('journey_stories'); ?>
      <a href="<?php echo get_site_url(); ?>/event">Book An Appointment</a>
    </div>
  </div>
</section>
<!-- video modal -->
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <iframe id="iframeYoutube" width="100%" height="400px" src="<?php the_field('video_link'); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
      
      
      <button type="button" class="btn btn-default modal-btn" data-dismiss="modal"><i class="fa fa-close"></i></button>
      
    </div>
  </div>
</div>
<?php endwhile;
else:
echo '<p>No content found</p>';
endif;
get_footer();
?>