<?php
/*
================================
     ADMIN ENQUEUE FUNCTIONS
================================
*/
function evibes_load_admin_scripts( $hook ){
     
     if('toplevel_page_custom_theme' !=$hook){
      return;
     }
    wp_register_style('evibes_admin', get_template_directory_uri() . '/assets/css/evibes-admin.css',array(),'1.0.0','all');
    wp_enqueue_style('evibes_admin');
    wp_register_style('evibes_admin', get_template_directory_uri() . '/assets/css/evibes.admin.css',array(),'1.0.0','all');
    wp_enqueue_style('evibes_admin');
    wp_enqueue_media();
    wp_register_script('evibes-admin-script', get_template_directory_uri() . '/assets/js/evibes-admin.js',array('jquery'),'1.0.0', true);
    wp_enqueue_script('evibes-admin-script');
    wp_register_script('evibes-admin-script1', get_template_directory_uri() . '/assets/js/evibes.admin.js',array('jquery'),'1.0.0', true);
    wp_enqueue_script('evibes-admin-script1');
    
}
add_action('admin_enqueue_scripts','evibes_load_admin_scripts');
