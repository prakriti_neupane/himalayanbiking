<?php
require_once('wp-bootstrap-navwalker.php');
require_once('inc/enqueue.php');

function biking(){
}
add_action('wp_enqueue_scripts', 'biking');
add_theme_support( 'post-thumbnails' );

//to add menubar
function biking_menu(){

}

add_action('after_setup_theme', 'biking_menu');

//registering navigation menu
register_nav_menus(array(
  'primary'=> __('Primary Menu'),
  'footer'=> __('Footer Menu'),
		//'sub_footer'=> __('Footer Sub Menu'),
));


//Post type function for News
add_action( 'init', 'create_events_posttype' );
add_theme_support('post-thumbnails');
add_post_type_support( 'events', 'thumbnail' ); 
function create_events_posttype() {
  register_post_type( 'events',
    array(
      'labels' => array(
        'name' => __( 'News & Events' ),
        'singular_name' => __( 'News & Events' )
      ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'events'),
    )
  );
}

add_action( 'init', 'create_biking_trial_posttype' );
add_theme_support('post-thumbnails');
add_post_type_support( 'biking_trial', 'thumbnail' ); 
function create_biking_trial_posttype() {
  register_post_type( 'biking_trail',
    array(
      'labels' => array(
        'name' => __( 'Biking Trial' ),
        'singular_name' => __( 'Biking Trail' )
      ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'biking_trail'),
    )
  );
}


function create_events_taxonomy() {
  $labels = array(
    'name'              => _x( 'News & Events', 'taxonomy general name', 'textdomain' ),
    'singular_name'     => _x( 'News_Events', 'taxonomy singular name', 'textdomain' ),
    'search_items'      => __( 'Search Events', 'textdomain' ),
    'all_items'         => __( 'All Events', 'textdomain' ),
    'parent_item'       => __( 'Parent Events', 'textdomain' ),
    'parent_item_colon' => __( 'Parent Events:', 'textdomain' ),
    'edit_item'         => __( 'Edit Events', 'textdomain' ),
    'update_item'       => __( 'Update Events', 'textdomain' ),
    'add_new_item'      => __( 'Add New Events Category', 'textdomain' ),
    'new_item_name'     => __( 'New Events Title', 'textdomain' ),
    'menu_name'         => __( 'Our Events', 'textdomain' ),
  );
  $args = array(
    'hierarchical'      => true,
    'labels'            => $labels,
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'events' ),
  );
  register_taxonomy( 'events', array('post','events','page'), $args );
  flush_rewrite_rules();
}
add_action( 'init','create_events_taxonomy');






if( function_exists('acf_add_options_page') ) {
  acf_add_options_page(array(
   'page_title'  => 'Theme General Settings',
   'menu_title' => 'Himalaya Biking Option',
   'menu_slug'  => 'theme-general-settings',
   'capability' => 'edit_posts',
   'redirect'  => false
 )
);
  acf_add_options_sub_page(array(
   'page_title'  => 'Theme Header Settings',
   'menu_title' => 'Header',
   'parent_slug' => 'theme-general-settings',
 )
);
  acf_add_options_sub_page(array(
   'page_title'  => 'Theme Footer Settings',
   'menu_title' => 'Footer',
   'parent_slug' => 'theme-general-settings',
 )
);
  acf_add_options_sub_page(array(
   'page_title'  => 'Know About Us Settings',
   'menu_title' => 'Know About Us Page',
   'parent_slug' => 'theme-general-settings',
 )
);
  acf_add_options_sub_page(array(
   'page_title'  => 'Theme Social Media Settings',
   'menu_title' => 'Social Media Link',
   'parent_slug' => 'theme-general-settings',
 )
);
  acf_add_options_sub_page(array(
   'page_title'  => 'Theme Hotel Settings',
   'menu_title' => 'Company Detail',
   'parent_slug' => 'theme-general-settings',
 )
);
}






?>