<!-- Footer Section Strat -->
<footer >
  
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="footer-logo">
          <img src="<?php the_field('footer_logo', option); ?>">
        </div>
        
        <div class="footer-contact">
          <ul>
            <li><?php the_field('address', option); ?></li>
            <li>Whatsapp/viber:  <?php the_field('phone_number', option); ?></li>
            <li>Email: <a href="mailto:outdoornagarkot@gmail.com"><?php the_field('email', option); ?></a></li>
          </ul>
        </div>
       <!--  <div class="email-col">
          <div class="input-group">
            <input class="input-lg" type="text" placeholder="Your Email Address">
            <span class="input-group-btn">
              <button class="btn btn-lg" type="button">SUBSCRIBE</button>
              
            </span>
          </div>
        </div> -->
        <div class="social-icon">
          
          <ul>
            <li><a class="tooltip-2" data-original-title="Facebook" href="<?php the_field('facebook_link', option); ?>" title="Facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a class="tooltip-2" data-original-title="Twitter" href="<?php the_field('twitter_link', option); ?>" title="Twitter"><i class="fa fa-twitter"></i></a></li>
            <li><a class="tooltip-2" data-original-title="Instagram" href="<?php the_field('instagram_link', option); ?>" title="Instagram"><i class="fa fa-instagram"></i></a></li>
            <li><a class="tooltip-2" data-original-title="Google Plus" href="<?php the_field('google_plus_link', option); ?>" title="Google Plus"><i class="fa fa-google-plus"></i></a></li>
          </ul>
        </div>
        
        
       
        
      </div>
    </div>
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="desclaimer">
        
<!--           <a href="#">Terms & Conditions</a> | <a href="#">Privacy Policy</a> | <a href="#" >Cookies Policy</a>
 -->          <p>&copy; 2017 Himlayan Biking Adventure  | 
          Designed &amp; Developed with <i class="fa fa-heart" style="color:red;">
            
          </style></i> by
          <a href="http://www.themenepal.com" target="_blank">
            Theme Nepal
          </a>
        </p>
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Footer Section End -->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/slick.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/typist-app.js"></script>
<script src="../dist/macy.js"></script>

      
 
<!----Banner Slider  ---->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.flexslider-min.js"></script>
<script type="text/javascript">
//Flexslider
jQuery('.flexslider').flexslider({
    animation: "slide",
    animationSpeed :100,
    animationLoop :true
  });


  // slick sdlider
    $(".variable").slick({
        dots: true,
        infinite: true,
        variableWidth: true,
        centerMode:true,
         nextArrow: '<i class="fa fa-angle-right"></i>',
        prevArrow: '<i class="fa fa-angle-left"></i>'
      });
    //----------------------fix nav on scroll
var stickyNavOffsetTop = $('#pav-mainnav').offset().top;
console.log(stickyNavOffsetTop);
var stickyNav = function(){
var scrollTop = $(window).scrollTop();
if( scrollTop > stickyNavOffsetTop ){
$('#pav-mainnav').stop().addClass('fixNav');
} else {
$('#pav-mainnav').stop().removeClass('fixNav');
}
};

// iframe js
function youframe() {
        var frameWidth = $('#video-container iframe').width();
        $('#video-container iframe').css('height', frameWidth *0.563); 
    }
    youframe();
    $(window).resize(youframe);


//fixnav on scsroll
var stickyNavOffsetTop = $('#pav-mainnav').offset().top;
//console.log(stickyNavOffsetTop);
var stickyNav = function(){
var scrollTop = $(window).scrollTop();
if( scrollTop > stickyNavOffsetTop ){
$('#pav-mainnav').stop().addClass('fixNav');
} else {
$('#pav-mainnav').stop().removeClass('fixNav');
}
};
// run our function on load
stickyNav();
// and run it again every time you scroll
$(window).scroll(function() {
stickyNav();
});

//lightbox js
var $lightbox = $('#lightbox');
$('[data-target="#lightbox"]').on('click', function(event) {
var $img = $(this).find('img'),
src = $img.attr('src'),
alt = $img.attr('alt'),
css = {
'maxWidth': $(window).width() - 100,
'maxHeight': $(window).height() - 100
};
$lightbox.find('img').attr('src', src);
$lightbox.find('img').attr('alt', alt);
$lightbox.find('img').css(css);
});
$lightbox.on('shown.bs.modal', function (e) {
var $img = $lightbox.find('img');
$lightbox.find('.modal-dialog').css({'width': $img.width()});
});

// video modal script
  $("#myModal").on("hidden.bs.modal",function(){
    $("#iframeYoutube").attr("src","#");
  })
function changeVideo(vId){
  var iframe=document.getElementById("iframeYoutube");
  iframe.src="https://www.youtube.com/embed/"+vId;

  $("#myModal").modal("show");
}



// scroll transition
$(window).scroll(function(){
  var wScroll =$(this).scrollTop();
    $('.cyclist').css({
      'transform' : 'translate(0px, -'+ wScroll /15 +'%)'
    });
     $('.abt-img').css({
      'transform' : 'translate(0px, '+ wScroll /15 +'%)'
    });
});
</script>
</body>
</html>